import java.util.Random;

/**
 * Classe que define o Curupira.
 * 
 * @author CWI Software
 * @version 21-09-2014 02:18
 */
public class Curupira extends Personagem
{
    
    /**
     * Construtor padrao do objeto Curupira.
     */
    public Curupira() {
        super();
        
        this.nome = "Curupira";
        
        // Player 1
        this.posicao[0] = -30;
        this.posicao[1] = -50;
        
        this.cor = "Red";
        
        this.itens = new String[2];
        this.itens[0] = new String("Lança");
        this.itens[1] = new String("Arco e Flecha");
        
        this.itemEquipado = this.itens[0];
        this.estaEquipado = true;
    }
    
    /**
     * Construtor do objeto Curupira.
     * 
     * @param String Nome do Personagem Curupira.
     * @param Double Vida do Personagem Curupira.
     * @param Int[] Posicao do Personagem Curupira.
     * @param String Cor do Personagem Curupira.
     * 
     */
    public Curupira(String nome, double vida, int[] posicao, String cor) {
        this();
        
        this.nome = nome;
        this.vida = vida;
        
        // Player ?
        this.posicao[0] = posicao[0];
        this.posicao[1] = posicao[1];
        
        this.cor = cor;
    }
    
    // Methods Abstract
    /**
     * Curupira ataca o alvo com um de seus itens:
     * 
     * Lança: Tira 5% da vida do alvo.
     * Arco e Flecha: Tira 7% da vida do alvo.
     * Não Equipado: Tira 3% da vida do alvo.
     * 
     * @param Personagem Alvo à atacar.
     */
    protected void atacar(Personagem alvo) {
        
        double dano;
        
        if(this.estaEquipado) {
            dano = this.itemEquipado.equals(this.itens[0]) ? 0.05 : 0.07;
        }else{
            dano = 0.03;
        }
        
       alvo.recebeAtaque(dano);
    }
    
    /**
     * Curupira executa um Combo Especial, perdendo seus itens e infligindo 40% de dano no alvo.
     * 
     * @param Personagem Alvo à atacar.
     */
    protected void executearComboEspecial(Personagem alvo) {
        this.itemEquipado = null;
        this.estaEquipado = false;
        this.itens = new String[0];
        
        double dano = 0.40;
        alvo.recebeAtaque(dano);
    }
    
    /**
     * Curupira contra ataca o alvo.
     * A chance de acerto do contra ataque é de 30%.
     * Caso acerte, o alvo recebe 60% de dano.
     * 
     * @param Personagem Alvo à atacar.
     */
    protected void contraAtacar(Personagem alvo) {
        Random random = new Random();
        
        int numero = random.nextInt(100);
        int chanceAtaque = 30;
        
        boolean acertouAtaque = numero <= chanceAtaque ? true : false;
        
        if(acertouAtaque) {
            double dano = 0.60;
            alvo.recebeAtaque(dano);
        }
    }
    
    /**
     * Movimenta o Curupira.
     * Como ele tem os pés voltados para tras, seu movimento é inverso:
     * 
     * Para frente o locomove para tras.
     * Para tras o locomove para frente.
     * Para cima seu movimento é normal.
     * 
     * @param Movimentar Direção que o curupira irá se movimentar.
     */
    public void movimentar(Movimentar movimento) {
        
        if(movimento == Movimentar.FRENTE) {
            this.posicao[0] -= 2;
        }else if(movimento == Movimentar.TRÁS) {
            this.posicao[0] += 2;
        }else if(movimento == Movimentar.CIMA) {
            this.posicao[1] += 3;
        }
        
    }
    
}
