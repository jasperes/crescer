
/**
 * Classe abstrata que define um Personagem.
 * 
 * @author CWI Software
 * @version 21-09-2014 01:39
 */
public abstract class Personagem
{
    protected String nome;
    protected double vida;
    protected int[] posicao;
    protected String cor;
    protected String[] itens;
    protected String itemEquipado;
    protected boolean estaEquipado;
    
    /**
     * Construtor do objeto Personagem.
     */
    public Personagem() {
        this.nome = "Sem Nome";
        this.vida = 100.0;
        this.posicao = new int[2];
        this.cor = "White";
        this.estaEquipado = false;
    }
    
    // Methods Get
    public String getNome() {
        return this.nome;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public int[] getposicao() {
        return this.posicao;
    }
    
    public int getPosicaoX() {
        return this.posicao[0];
    }
    
    public int getPosicaoY() {
        return this.posicao[1];
    }
    
    public String getCor() {
        return this.cor;
    }
    
    // Methods Set
    
    // Methods Abstract
    protected abstract void atacar(Personagem alvo);
    
    protected abstract void executearComboEspecial(Personagem alvo);
    
    protected abstract void contraAtacar(Personagem alvo);
    
    public abstract void movimentar(Movimentar movimento);

    /*
        protected void atacar(Personagem alvo) {
        
    }
    
    protected void executearComboEspecial(Personagem alvo) {
        
    }
    
    protected void contraAtacar(Personagem alvo) {
        
    }
    
    public void movimentar(Movimentar movimento) {
        if(movimento == Movimentar.FRENTE) {
            this.posicao[0] += 2;
        }else if(movimento == Movimentar.TRÁS) {
            this.posicao[0] -= 2;
        }else if(movimento == Movimentar.CIMA) {
            this.posicao[1] += 3;
        }
    }
    */
    
    
    // Methods
    /**
     * Personagem recebe dano, diminuindo sua vida.
     * 
     * @param double Dano à ser infligido no Personagem.
     */
    public void recebeAtaque(double dano) {
        this.vida *= dano;
    }
    
}
