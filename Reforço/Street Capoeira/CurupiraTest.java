

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Cenarios de teste para a classe Curupira.
 *
 * @author  CWI Software
 * @version 22-09-2014 16-03
 */
public class CurupiraTest
{
    // Curupira()
    @Test public void quandoCriarCurupiraPadraoNascerNaPosicaoDoPlayerUm() {
        Personagem curupira = new Curupira();
        
        int posicaoEsperadoX = -30;
        int posicaoEsperadoY = -50;
        
        int posicaoObtidoX = curupira.getPosicaoX();
        int posicaoObtidoY = curupira.getPosicaoY();
        
        assertEquals(posicaoEsperadoX, posicaoObtidoX);
        assertEquals(posicaoEsperadoY, posicaoObtidoY);
    }
    
    @Test public void quandoCriarCurupiraPadraoNascerVermelhor() {
        Personagem curupira = new Curupira();
        
        String corEsperado = "red";
        
        String corObtido = curupira.getCor();
        
        assertEquals(corEsperado, corObtido);
    }
    
    @Test public void quandoCriarCurupiraPadraoNascerComLancaArcoEFlecha() {
        Personagem curupira = new Curupira();
        
        
    }
}
