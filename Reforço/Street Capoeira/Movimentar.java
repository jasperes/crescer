
/**
 * Classe Enum "Movimentar" que define os movimentos dos personagens.
 * 
 * @author CWI Software
 * @version 21-09-2014 02:04
 */
public enum Movimentar
{
    FRENTE, TRÁS, CIMA
}
