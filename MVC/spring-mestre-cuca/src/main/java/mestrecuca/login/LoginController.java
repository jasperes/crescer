package mestrecuca.login;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String home(Model model){
		model.addAttribute("mensagem", "");
		return "login.html";
	}
	
	@RequestMapping(value = "/autentica", method = RequestMethod.POST)
	public String autentica(Usuario usuario, Model model, HttpSession session){
		if(credenciaisUsuarioValidas(usuario)){
			session.setAttribute("usuarioLogado", usuario);
			return "home.html";
		}else{
			model.addAttribute("mensagem", "Usuário ou senha inválidos");
			return "login.html";
		}
	}
	
	@RequestMapping(value="/calcular-juros", method = RequestMethod.GET)
	public String calculaJuros(@RequestParam("txJuros") double txJuros, Model model){
		model.addAttribute("mensagem", "Deu certo " + txJuros);
		return "login.html";
	}

	private boolean credenciaisUsuarioValidas(Usuario usuario) {
		return usuario.getUsuario().equals("admin") && usuario.getSenha().equals("123");
	} 
}
