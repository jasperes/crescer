package mestrecuca.mercadoria;

public class Mercadoria {

	private Integer idMercadoria;
	
	private String descricao;
	
	private Integer peso;
	
	public Integer getPeso() {
		return peso;
	}
	
	public void setPeso(Integer peso) {
		this.peso = peso;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getIdMercadoria() {
		return idMercadoria;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setIdMercadoria(Integer idMercadoria) {
		this.idMercadoria = idMercadoria;
	}
}
