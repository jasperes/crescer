package mestrecuca.mercadoria;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MercadoriaController {

	@Inject
	private MercadoriaDao mercadoriaDao;
	
	@RequestMapping(value = "/mercadorias", method = RequestMethod.GET)
	public String abrePaginaMercadorias(Model model){
		List<Mercadoria> listaMercadorias = mercadoriaDao.buscaTodasMercadorias();
		model.addAttribute("mercadorias", listaMercadorias);
		model.addAttribute("nome", "MARLON");
		return "mercadorias.html";
	}

	@RequestMapping(value = "/pedaco-mercadorias", method = RequestMethod.GET)
	public String abrePedacoMercadorias(@RequestBody Mercadoria mercadoria, Model model){
		List<Mercadoria> listaMercadorias = mercadoriaDao.buscaTodasMercadorias();
		model.addAttribute("mercadorias", listaMercadorias);
		model.addAttribute("nome", "MARLON");
		return "pedaco-mercadorias.html";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@ResponseBody
	@RequestMapping(value = "/mercadorias/json", method = RequestMethod.GET)
	public List<Mercadoria> buscaMercadorias(Model model){
		return mercadoriaDao.buscaTodasMercadorias();
	}

	@ResponseBody
	@RequestMapping(value = "/mercadorias/salvar", method = RequestMethod.POST)
	public List<Mercadoria> salvarEBuscarMercadorias(@RequestBody Mercadoria mercadoria, Model model){
		mercadoriaDao.inserirMercadoria(mercadoria);
		return mercadoriaDao.buscaTodasMercadorias();
	}
	
	
}
