package mestrecuca.mercadoria;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class MercadoriaDao {

	private JdbcTemplate jdbc;
	
	@Inject
	public MercadoriaDao(DataSource datasource){
		jdbc = new JdbcTemplate(datasource);
	}
	
	public void inserirMercadoria(Mercadoria mercadoria){
		String sql = "INSERT INTO Mercadoria "
				+ "(idMercadoria, descricao, peso, codigoRastreamento) "
				+ "VALUES (MERCADORIA_SEQ.NEXTVAL, ?, ?, ?)";
		
		jdbc.update(sql, mercadoria.getDescricao(), mercadoria.getPeso(), "abc");
	}
	
	public void atualizarMercadoria(Mercadoria mercadoria){
		String sql = "UPDATE Mercadoria SET descricao = ? WHERE idMercadoria = ?";
		jdbc.update(sql, mercadoria.getDescricao(), mercadoria.getIdMercadoria());
	}
	
	public void excluirMercadoria(Integer idMercadoria){
		String sql = "DELETE FROM Mercadoria WHERE idMercadoria = ?";
		jdbc.update(sql, idMercadoria);
	}
	
	public Integer getTotalMercadorias(){
		return jdbc.queryForObject("SELECT COUNT(*) From Mercadoria", Integer.class);
	}
	
	public List<Mercadoria> buscaTodasMercadorias(){
		return jdbc.query("SELECT * FROM Mercadoria", new RowMapper<Mercadoria>(){

			@Override
			public Mercadoria mapRow(ResultSet rs, int rowNum) throws SQLException {
				Mercadoria mercadoria = new Mercadoria();
				mercadoria.setDescricao(rs.getString("descricao"));
				mercadoria.setIdMercadoria(rs.getInt("idMercadoria"));
				return mercadoria;
			}
			
		});
	}

	
	
	
}
