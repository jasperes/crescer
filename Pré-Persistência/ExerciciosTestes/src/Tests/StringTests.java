package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringTests {

	@Test public void charAt() {
		String string = "abc";
		
		char esperado = 'c';
		char obtido = string.charAt(2);
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void startsWith() {
		String string = "abc";
		
		boolean esperado = true;
		boolean obtido = string.startsWith("a");
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void endsWith() {
		String string = "abc";
		
		boolean esperado = true;
		boolean obtido = string.endsWith("c");
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void contains() {
		String string = "abc";
		
		boolean esperado = true;
		boolean obtido = string.contains("b");
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void compareTo() {
		String string = "abc";
		
		int esperado = 0;
		int obtido = string.compareTo("abc");
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void indexOf() {
		String string = "frase tosca";
		int esperado = 3;
		int obtido = string.indexOf("s");
		assertEquals(esperado, obtido);
	}
	
	@Test public void toLowerCase() {
		String string = "ABC";
		
		String esperado = "abc";
		String obtido = string.toLowerCase();
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void toUpperCase() {
		String string = "abc";
		
		String esperado = "ABC";
		String obtido = string.toUpperCase();
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void length() {
		String string = "abcdefghijklmnopqrstuvwxyz";
		
		int esperado = 26;
		int obtido = string.length();
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void replace() {
		String string = "outra frase tosca";
		String esperado = "outra frase linda";
		String obtido = string.replace("tosca", "linda");
		assertEquals(esperado, obtido);
	}
	
	@Test public void split() {
		String string = "frase aleatória";
		String[] string2 = string.split(" ");
		
		String esperado1 = "frase";
		String obtido1 = string2[0];
		
		String esperado2 = "aleatória";
		String obtido2 = string2[1];
		
		assertEquals(esperado1, obtido1);
		assertEquals(esperado2, obtido2);
	}

}
