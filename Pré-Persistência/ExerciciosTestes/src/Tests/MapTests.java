package Tests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class MapTests {

	@Test public void putAndGet() {
		Map<Integer, String> map = new HashMap<>();
		map.put(1, "1");
		
		String esperado = "1";
		String obtido = map.get(1);
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void containsKey() {
		Map<Integer, String> map = new HashMap<>();
		map.put(1, "A");
		
		boolean esperado = true;
		boolean obtido = map.containsKey(1);
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void containsValue() {
		Map<Integer, String> map = new HashMap<>();
		map.put(1, "Potato");
		
		boolean esperado = true;
		boolean obtido = map.containsValue("Potato");
		
		assertEquals(esperado, obtido);
	}

}
