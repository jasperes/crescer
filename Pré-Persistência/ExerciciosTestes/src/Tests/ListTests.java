package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListTests {

	@Test public void get() {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		
		int esperado = 1;
		int obtido = list.get(0);
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void set() {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.set(0, 2);
		
		int esperado = 2;
		int obtido = list.get(0);
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void indexOf() {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		
		int esperado = 0;
		int obtido = list.indexOf(1);
		
		assertEquals(esperado, obtido);
	}

}
