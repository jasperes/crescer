package Filmator;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class FilmeTests {
	
	// Filme()
	
	// adicionarAtorAoElenco()
	@Test public void quandoAdicionarAtorAoElencoAumentarElencoEmUm() {
		Ator ator = new Ator("Nome", "Masculino");
		Filme filme = new Filme("Nome", Genero.AÇÃO, "");
		filme.adicionarAtorAoElenco(ator);
		
		ArrayList<Ator> esperado = new ArrayList<Ator>();
		esperado.add(ator);
		
		ArrayList<Ator> obtido = filme.getElenco();
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void quandoAdicionarVariosAtoresAumentarElenco() {
		Ator ator1 = new Ator("João", "Masculino");
		Ator ator2 = new Ator("Pedro", "Masculino");
		Ator ator3 = new Ator("Maria", "Feminino");
		
		Filme filme = new Filme("Nome", Genero.AÇÃO, "");
		
		filme.adicionarAtorAoElenco(ator1);
		filme.adicionarAtorAoElenco(ator2);
		filme.adicionarAtorAoElenco(ator3);
		
		ArrayList<Ator> esperado = new ArrayList<Ator>();
		esperado.add(ator1);
		esperado.add(ator2);
		esperado.add(ator3);
		
		ArrayList<Ator> obtido = filme.getElenco();
		
		assertEquals(esperado, obtido);
	}
}
