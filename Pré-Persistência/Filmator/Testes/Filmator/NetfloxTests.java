package Filmator;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class NetfloxTests {

	// adicionarFilme()
	@Test public void quandoAdicionarFilmeGravarNaLista() {
		Netflox netflox = new Netflox();
		Filme filme = new Filme("", Genero.AÇÃO, "");
		
		netflox.adicionarFilme(filme);
		
		ArrayList<Filme> filmeEsperado = new ArrayList<>();
		filmeEsperado.add(filme);
		ArrayList<Filme> filmeObtido = netflox.getFilmes();
		
		assertEquals(filmeEsperado, filmeObtido);
	}
	
	@Test public void quandoAdicionarTresFilmesGravarNaLista() {
		Netflox netflox = new Netflox();
		
		Filme filme1 = new Filme("", Genero.AÇÃO, "");
		Filme filme2 = new Filme("", Genero.COMÉDIA, "");
		Filme filme3 = new Filme("", Genero.ROMANCE, "");

		netflox.adicionarFilme(filme1);
		netflox.adicionarFilme(filme2);
		netflox.adicionarFilme(filme3);
		
		ArrayList<Filme> filmeEsperado = new ArrayList<>();
		filmeEsperado.add(filme1);
		filmeEsperado.add(filme2);
		filmeEsperado.add(filme3);
		
		ArrayList<Filme> filmeObtido = netflox.getFilmes();
		
		assertEquals(filmeEsperado, filmeObtido);
		
	}
	
	// reproduzir()
	// - Contador
	@Test public void quandoReproduzirAumentarContador() {
		Netflox netflox = new Netflox();
		Filme filme = new Filme("", Genero.AÇÃO, "");
		netflox.adicionarFilme(filme);
		netflox.reproduzir(filme);
		
		
		int esperado = 1;
		int obtido = netflox.getVezesGeneroAssistido(Genero.AÇÃO);
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void quandoReproduzirTresFilmesAumentarContador() {
		Netflox netflox = new Netflox();
		
		Filme filme1 = new Filme("", Genero.AÇÃO, "");
		Filme filme2 = new Filme("", Genero.AÇÃO, "");
		Filme filme3 = new Filme("", Genero.AÇÃO, "");

		netflox.adicionarFilme(filme1);
		netflox.adicionarFilme(filme2);
		netflox.adicionarFilme(filme3);
		
		netflox.reproduzir(filme1);
		netflox.reproduzir(filme2);
		netflox.reproduzir(filme3);
		
		int esperado = 3;
		int obtido = netflox.getVezesGeneroAssistido(Genero.AÇÃO);
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void quandoAssistidoFilmesDeGenerosDiferentes() {
		Netflox netflox = new Netflox();
		
		Filme filme1 = new Filme("", Genero.AÇÃO, "");
		Filme filme2 = new Filme("", Genero.SUSPENSE, "");

		netflox.adicionarFilme(filme1);
		netflox.adicionarFilme(filme2);
		
		netflox.reproduzir(filme1);
		netflox.reproduzir(filme2);

		int esperado1 = 1;
		int obtido1 = netflox.getVezesGeneroAssistido(Genero.AÇÃO);
		
		int esperado2 = 1;
		int obtido2 = netflox.getVezesGeneroAssistido(Genero.SUSPENSE);
		
		assertEquals(esperado1, obtido1);
		assertEquals(esperado2, obtido2);
	}
	
	@Test public void quandoAssistidoVariosFilmesDeGenerosDiferentes() {
		Netflox netflox = new Netflox();
		
		Filme filme1 = new Filme("", Genero.AÇÃO, "");
		Filme filme2 = new Filme("", Genero.SUSPENSE, "");
		Filme filme3 = new Filme("", Genero.SUSPENSE, "");
		Filme filme4 = new Filme("", Genero.SUSPENSE, "");
		Filme filme5 = new Filme("", Genero.COMÉDIA, "");
		Filme filme6 = new Filme("", Genero.COMÉDIA, "");

		netflox.adicionarFilme(filme1);
		netflox.adicionarFilme(filme2);
		netflox.adicionarFilme(filme3);
		netflox.adicionarFilme(filme4);
		netflox.adicionarFilme(filme5);
		netflox.adicionarFilme(filme6);
		
		netflox.reproduzir(filme1);
		netflox.reproduzir(filme2);
		netflox.reproduzir(filme3);
		netflox.reproduzir(filme4);
		netflox.reproduzir(filme5);
		netflox.reproduzir(filme6);

		int esperado1 = 1;
		int obtido1 = netflox.getVezesGeneroAssistido(Genero.AÇÃO);

		int esperado2 = 3;
		int obtido2 = netflox.getVezesGeneroAssistido(Genero.SUSPENSE);
		
		int esperado3 = 2;
		int obtido3 = netflox.getVezesGeneroAssistido(Genero.COMÉDIA);
		
		assertEquals(esperado1, obtido1);
		assertEquals(esperado2, obtido2);
		assertEquals(esperado3, obtido3);
	}

}
