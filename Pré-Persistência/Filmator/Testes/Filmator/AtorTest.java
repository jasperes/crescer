package Filmator;

import static org.junit.Assert.*;

import org.junit.Test;

public class AtorTest {

	// Ator()
	@Test public void quandoCriarAtorGravarNome() {
		Ator ator = new Ator("Nome", "Masculino");
		
		String esperado = "Nome";
		String obtido = ator.getNome();
		
		assertEquals(esperado, obtido);
	}

	
	@Test public void quandoCriarAtorGravarSexo() {
		Ator ator = new Ator("Nome", "Masculino");
		
		String esperado = "Masculino";
		String obtido = ator.getSexo();
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void quandoCriarVariosAtoresGravarDadosCorretamente() {
		
		String[] nomes = new String[5];
		String[] sexos = new String[5];
		Ator[] ator = new Ator[5];
		
		nomes[0] = "Jo�o";
		nomes[1] = "Maria";
		nomes[2] = "Marcela";
		nomes[3] = "Pedro";
		nomes[4] = "Joana";
		
		sexos[0] = "Masculino";
		sexos[1] = "Feminino";
		sexos[2] = "Feminino";
		sexos[3] = "Masculino";
		sexos[4] = "Feminino";
		
		ator[0] = new Ator(nomes[0], sexos[0]);
		ator[1] = new Ator(nomes[1], sexos[1]);
		ator[2] = new Ator(nomes[2], sexos[2]);
		ator[3] = new Ator(nomes[3], sexos[3]);
		ator[4] = new Ator(nomes[4], sexos[4]);
		
		for(int i = 0; i < 5; i++) {
			assertEquals(nomes[i], ator[i].getNome());
			assertEquals(sexos[i], ator[i].getSexo());
		}
	}
	
	// toString()
	@Test public void quandoCriarAtorVerificarToString() {
		Ator ator = new Ator("Jo�o", "Masculino");
		
		String esperado = "Ator chamado Jo�o.";
		String obtido = ator.toString();
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void quandoCriarVariosAtoresVerificarToString() {
		Ator[] ator = new Ator[5];
		String[] esperado = new String[5];
		
		ator[0] = new Ator("Jo�o", "Masculino");
		ator[1] = new Ator("Maria", "Feminino");
		ator[2] = new Ator("Marcela", "Feminino");
		ator[3] = new Ator("Pedro", "Masculino");
		ator[4] = new Ator("Joana", "Feminino");
		
		esperado[0] = "Ator chamado Jo�o.";
		esperado[1] = "Atriz chamada Maria.";
		esperado[2] = "Atriz chamada Marcela.";
		esperado[3] = "Ator chamado Pedro.";
		esperado[4] = "Atriz chamada Joana.";
		
		for(int i = 0; i < 5; i++) {
			assertEquals(esperado[i], ator[i].toString());
		}
	}
}
