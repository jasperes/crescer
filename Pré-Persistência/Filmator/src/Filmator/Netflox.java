package Filmator;

import java.util.ArrayList;
import java.util.HashMap;

public class Netflox {
	private ArrayList<Filme> filmes;
	private HashMap<Genero, Integer> contador;
	
	/**
	 * Contrutor do objeto Netflox.
	 */
	public Netflox() {
		this.filmes = new ArrayList<>();
		this.contador = new HashMap<>();
	}
	
	// Methods Get
	public ArrayList<Filme> getFilmes() {
		return this.filmes;
	}
	
	public int getVezesGeneroAssistido(Genero genero) {
		return this.contador.get(genero);
	}
	
	public HashMap<Genero, Integer> getListaDeVezesGenerosAssistidos() {
		return this.contador;
	}
	
	// Methods
	/**
	 * Adicionar um filme na lista de filmes do Netflox.
	 * 
	 * @param filme Filme à ser incluído na lista.
	 */
	public void adicionarFilme(Filme filme) {
		this.filmes.add(filme);
	}
	
	/**
	 * Reproduz o filme e adiciona o genero no contador de vezes assistido por genero.
	 * 
	 * @param filme Filme à ser reproduzido.
	 */
	public void reproduzir(Filme filme) {
		// Algo pra reproduzir...
		
		/*
		 * Adiciona +1 no contador de quantas vezes foi assistido cada genero.
		 * 
		 * 1 - Cria variavel para verificar se o genero já foi incluído no contador.
		 * 2 - Se o genero existe:
		 * 	   - Pega contador no genero e incrementa +1;
		 * 	   - Grava no contador nova contagem.
		 * 	   Se o genero não existe:
		 * 	   - Inclui genero no contador;
		 */
		
		boolean generoExiste = this.contador.containsKey(filme.getGenero());
        
        if(generoExiste) {
            int contador = this.contador.get(filme.getGenero()) + 1;
            this.contador.put(filme.getGenero(), contador);
        }else{
        	this.contador.put(filme.getGenero(), 1);
        }
	}
}
