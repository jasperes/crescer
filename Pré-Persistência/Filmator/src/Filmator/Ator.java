package Filmator;

public class Ator {
	
	private String nome;
	private String sexo;
	
	/**
	 * Construtor do objeto Ator.
	 * 
	 * @param nome Nome do Ator ou Atriz.
	 * @param sexo Sexo do Ator ou Atriz.
	 */
	public Ator(String nome, String sexo) {
		this.nome = nome;
		this.sexo = sexo;
	}
	
	// Methods Get
	public String getNome() {
		return this.nome;
	}
	
	public String getSexo() {
		return this.sexo;
	}
	
	// Methods
	public String toString() {
		StringBuilder string = new StringBuilder();
		
		string.append(
			String.format("%s chamad%s %s.",
				this.sexo == "Masculino" ? "Ator" : "Atriz",
				this.sexo == "Masculino" ? "o" : "a",
				this.nome));
		
		return string.toString();
	}

}
