package Filmator;

import java.util.ArrayList;

public class Filme {
	private String nome;
	private Genero genero;
	private String sobre;
	private ArrayList<Ator> elenco;
	
	/**
	 * Construtor do objeto Filme.
	 * 
	 * @param nome Nome do filme.
	 * @param genero Genero do filme.
	 * @param sobre Descrição do filme.
	 */
	public Filme(String nome, Genero genero, String sobre) {
		this.nome = nome;
		this.genero = genero;
		this.sobre = sobre;
		elenco = new ArrayList<Ator>();
	}
	
	// Methods Get
	public String getNome() {
		return this.nome;
	}
	
	public Genero getGenero() {
		return this.genero;
	}
	
	public String sobre() {
		return this.sobre;
	}
	
	public ArrayList<Ator> getElenco(){
		return this.elenco;
	}
	
	//Methods
	/**
	 * Adiciona um ator ou atriz ao elenco do filme.
	 * 
	 * @param ator Ator ou atriz � adicionar no elenco.
	 */
	public void adicionarAtorAoElenco(Ator ator) {
		this.elenco.add(ator);
	}
	
	/**
	 * Pesquisa na lista do elenco os atores e atrizes com um determinado nome.
	 * 
	 * @param nome Nome do ator ou atriz que est� procurando.
	 * @return atores Lista de atores e atrizes quem possuem o nome buscado.
	 */
	public ArrayList<Ator> procurarAtor(String nome) {
		ArrayList<Ator> atores = new ArrayList<Ator>();
		
		for(Ator ator : elenco) {
			if(ator.getNome().toLowerCase().contains(nome.toLowerCase())) {
				atores.add(ator);
			}
		}
		
		return atores;
	}
	
	public String toString() {
		StringBuilder string = new StringBuilder();
		
		string.append(
			String.format("%s\n\n%s\n\nElenco:\n",
					this.nome,
					this.sobre));
		
		for(Ator ator : elenco) {
			string.append(ator.getNome());
			string.append("\n");
		}
		
		return string.toString();
	}
	
}