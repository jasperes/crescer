package sortiator;

import javax.swing.JOptionPane;

public class Sortiator {
	
	public static void main(String[] args) {
		
		String playerOne = JOptionPane.showInputDialog("Player One Name: ");
		String playerTwo = JOptionPane.showInputDialog("Player Two Name: ");
		
		String winner = Math.random() > 0.5 ? playerOne : playerTwo;
		
		JOptionPane.showMessageDialog(null, "Player " + winner + " Winn!");
	}
}
