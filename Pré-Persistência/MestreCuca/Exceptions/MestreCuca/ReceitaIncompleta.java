package MestreCuca;

public class ReceitaIncompleta extends RuntimeException {
	private String message;

	public ReceitaIncompleta(String message) {
		super(message);
	}
}
