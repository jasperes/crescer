package MestreCuca;

public enum UnidadeDeMedida {
	
	//NENHUMA_UNIDADE("",""),
	UNIDADE("Unidade", "Unidades"),
	GRAMAS("Gramas", "Gramas"),
	MILIGRAMAS("Miligramas", "Miligramas"),
	KILOGRAMAS("Kilogramas", "Kilogramas"),
	LITROS("Litros", "Litros"),
	COLHER_DE_SOPA("Colher de sopa", "Colheres de sopa"),
	DOSE("Dose", "Doses");

	private String descricaoSingular;
	private String descricaoPlural;
	
	private UnidadeDeMedida(String descricaoSingular, String descricaoPlural) {
		this.descricaoSingular = descricaoSingular;
		this.descricaoPlural = descricaoPlural;
	}
	
	public String getDescricao() {
		return this.descricaoSingular;
	}
	
	public String getDescricaoPlural() {
		return this.descricaoPlural;
	}
	
}
