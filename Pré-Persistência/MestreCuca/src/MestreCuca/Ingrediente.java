package MestreCuca;

import mestrecuca.ingrediente.UnidadeDeMedida;

public class Ingrediente {
	private int idIngrediente;
	private UnidadeDeMedida unidadeMedida;
	private String nome;
	private double quantidade;
	private double preco;

	/**
	 * Construtor do objeto Ingrediente.
	 * 
	 * @param unidadeMedida
	 *            Tipo de unidade de medida.
	 * @param nome
	 *            Nome do ingrediente.
	 * @param quantidade
	 *            Quantidade do ingrediente na unidade de medida.
	 */
	public Ingrediente(UnidadeDeMedida unidadeMedida, String nome,
			double quantidade) {
		this.unidadeMedida = unidadeMedida;
		this.nome = nome;
		this.quantidade = quantidade;
	}

	// Metodos Get
	public int getIdIngrediente() {
		return idIngrediente;
	}

	public UnidadeDeMedida getUnidadeMedida() {
		return this.unidadeMedida;
	}

	public String getNome() {
		return this.nome;
	}

	public double getQuantidade() {
		return this.quantidade;
	}

	public double getPreco() {
		return this.preco;
	}

	// Metodos Set
	public void setIdIngrediente(int idIngrediente) {
		this.idIngrediente = idIngrediente;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}
}
