Create tablespace mestrecuca_dat 
  datafile '/lib/oracle/oradata/XE/mestrecuca01.dbf' 
  size 100m 
  autoextend on next 100m 
  maxsize 2048m;

create user mestrecuca 
  identified by mestrecuca 
  default tablespace mestrecuca_dat;

grant connect, resource, create view to mestrecuca;

create table receita (
  IdReceita	integer		not null,
  Nome		varchar2(50)	not null,
  constraint	PK_Receita	primary key	(IdReceita)
);

create table instrucao (
  IdInstrucao	integer		not null,
  Descricao	clob		not null,
  IdReceita	integer		not null,
  constraint	PK_Instrucao	primary key	(IdInstrucao),
  constraint	FK_Instrucao_Receita		foreign key	(IdReceita)
    references	Receita (IdReceita)
);

create table ingrediente (
  IdIngrediente	integer		not null,
  Nome		varchar2(50)	not null,
  Quantidade	number(5,2)	not null,
  IdReceita	integer		not null,
  constraint	PK_Ingrediente	primary key	(IdIngrediente),
  constraint	FK_Ingrediente_Receita		foreign key	(IdReceita)
    references	Receita (IdReceita)
);

create table usuario (
  IdUsuario	integer		not null,
  Usuario	varchar2(50)	not null,
  Senha		varchar2(20)	not null,
  constraint	PK_Usuario	primary key	(IdUsuario)
);