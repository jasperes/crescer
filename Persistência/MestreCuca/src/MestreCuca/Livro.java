package MestreCuca;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Livro implements LivroReceitas {
	//private List<Receita> receitas;
	private String jsonFile;
	
	/**
	 * Contrutor do objeto Livro.
	 * @throws IOException Exception ao tentar criar o arquivo json
	 */
	public Livro() throws IOException {
		//this.receitas = new ArrayList<>();
		this.jsonFile = "livro.json";
	}

	// Metodos Get
	public List<Receita> getTodasReceitas() {
		return this.lerReceitas();
	}
	
	public Receita buscaReceitaPeloNome(String nome) {
		
		for(Receita receita : this.lerReceitas()) {
			
			String nomeReceita = receita.getNome().toLowerCase();
			boolean receitaEncontrada = nomeReceita.equalsIgnoreCase(nome);
			
			if(receitaEncontrada) {
				return receita;
			}
		}
		
		throw new ReceitaIncompleta("Receita não encontrada.");
	}
	
	/**
	 * Calcula o preço total das receitas no livro.
	 * 
	 * @return double Preço total das receitas.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public double getPrecoTotalReceitas() {
		double precoTotal = 0;
		for(Receita receita : this.lerReceitas()) {
			precoTotal += receita.getPrecoTotal();
		}
		return precoTotal;
	}
	
	/**
	 * Verifica as receitas sem determinados ingredientes.
	 * 
	 * @param ingredientes Ingredientes que não devem constar nas receitas.
	 * @return List<Receita> Receitas sem os ingredientes informados.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public List<Receita> getReceitasSemDeterminadosIngredientes(List<Ingrediente> ingredientes) {
		List<Receita> receitasSemIngredientes = new ArrayList<>();
		
		for(Ingrediente ingrediente : ingredientes) {
			for(Receita receita : this.lerReceitas()) {
				
				List<Ingrediente> ingredienteReceita = receita.getIngredientes();
				boolean receitaNaoTemIngrediente = !(ingredienteReceita.contains(ingrediente));
				
				if(receitaNaoTemIngrediente) {
					receitasSemIngredientes.add(receita);
				}
			}
		}
		return receitasSemIngredientes;
	}
	
	/**
	 * Agrupa os ingredientes de cada receita do Livro por: 
	 * Nome e UnidadeDeMedida, somando a quantidade total.
	 * 
	 * @return List<String> Lista de Strings com o nome, unidade de medida 
	 * e a quantidade total de cada ingrediente das receitas do livro.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public List<String> getQuantidadeTotalIngredientes() {
		
		// lógica aplicada nesse código: Empacota, Extrai e Empacota;
		// agrupa os dados necessários dos ingredientes em uma variavel, sendo eles:
		// Grupos de Nome/Unidade de Medida, com a quantidade total deste;
		// depois, pega os dados dessa variavel e salva como string em uma lista que será retornada.
		
		// cria variaveis com:
		// nome e unidade de medida dos ingredientes;
		// quantidade total das medias, agrupando por nome e unidade de medida;
		HashMap<List<String>, Double> quantidadeTotal = new HashMap<>();

		// cria loop:
		// para cada receita da lista de receitas do Livro;
		// verifica cada ingrediente da receita.
		for(Receita receita : this.lerReceitas()) {
			for(Ingrediente ingrediente : receita.getIngredientes()) {
				
				// pega valores dos ingredientes
				String nome = ingrediente.getNome();
				String unidade = ingrediente.getUnidadeMedida().name();
				double quantidade = ingrediente.getQuantidade();
				
				// cria uma lista com os dados do ingrediente (nome + unidade)
				List<String> dadosIngrediente = new ArrayList<>();
				dadosIngrediente.add(nome);
				dadosIngrediente.add(unidade);
				
				// verifica se o ingrediente não existe na lista de quantidades totais de cada ingrediente;
				// Caso não existir, cria e inseri a quantidade;
				// Do contrário, pega a quantidade atual gravada, soma com a quantidade do ingrediente deste for
				//		em seguida, adiciona o novo valor na lista de quantidade total dos ingredientes.
				boolean ingredienteNaoAdicionado = !quantidadeTotal.containsKey(dadosIngrediente);

				if(ingredienteNaoAdicionado) {
					quantidadeTotal.put(dadosIngrediente, quantidade);
				}else{
					double novaQuantidade = quantidadeTotal.get(dadosIngrediente) + quantidade;
					quantidadeTotal.put(dadosIngrediente, novaQuantidade);
				}
			}
		}
		
		// após gravar as quantidades de cada ingrediente, agrupando por Nome e UnidadeDeMedida;
		// cria lista de String com:
		// quantidade - unidade de medida - nome
		//
		// Exemplo:
		// 500 Kilogramas de Chocolate
		List<String> dadosTotais = new ArrayList<>();
		// para cada entrada no HashMap quantidadeTotal:
		for(Map.Entry<List<String>, Double> ingrediente : quantidadeTotal.entrySet()) {
			// grava em variavel a quantidade de duas formas:
			// para utilizar na String;
			// para consulta no momento de utilizar na String.
			String quantidade = ingrediente.getValue().toString();
			
			// grava em variaveis os dados do ingrediente
			// nome do ingrediente
			// unidade do ingrediente
			List<String> dados = ingrediente.getKey();
			String nome = dados.get(0);
			
			// para montar a String corretamente, 
			// é feito uma verificação se a unidade deve estar no singular ou plural
			// com 4 variaveis:
			// quantidade do ingrediente, em double, para verificação do tamanho;
			// string unidade gravada na lista, está no singular;
			// UnidadeDeMedida da string unidade, para fazer o teste de singular ou plural;
			// String final unidade, fazendo a verificação se deve estar em singular ou plural;
			double qnt = ingrediente.getValue().intValue();
			String unidadeSingular = dados.get(1);
			UnidadeDeMedida unidadeTeste = UnidadeDeMedida.valueOf(unidadeSingular);//UnidadeDeMedida.NENHUMA_UNIDADE.procuraPorDescricao(unidadeSingular);
			String unidade = qnt > 1.0 ? unidadeTeste.getDescricaoPlural() : unidadeTeste.getDescricao();
			
			// cria e projeta a String desta entrada para gravar na lista de Strings
			// a String será salva como:
			// quantidade do ingrediente;
			// unidade do ingrediente;
			// caso a quantidade seja maior que zero 
			//		e a ultima letra da unidade já não esteja no plural (com s) 
			//		adiciona a letra s no final da unidade;
			// palavra 'de' e o nome do ingrediente
			// ficando como nesse exemplo: 500 Kilogramas de Chocolate.
			StringBuilder string = new StringBuilder();
			
			string.append(
					String.format("%s %s de %s.",
							quantidade,
							unidade,
							nome)
					);
			// adiciona a String na lista de Strings.
			dadosTotais.add(string.toString());
		}
		// Por fim, retorna os a lista de Strings.
		return dadosTotais;
	}
	
	// Metodos de alteração da List receitas
	/**
	 * Inseri uma receita na lista de receitas.
	 * 
	 * @param receita Receita à ser inserida na lista de receitas.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void inserir(Receita receita) {
		
		boolean nomeNaoNull = receita.getNome() != null;
		boolean nomeNaoVazio = !receita.getNome().isEmpty(); 
		
		if(nomeNaoNull && nomeNaoVazio) {
			//this.receitas.add(receita);
			this.escreverReceita(receita);
		}
	}
	
	/**
	 * Atualiza uma receita na lista de receitas.
	 * 
	 * @param nome Nome da receita à atualizar.
	 * @param receitaAtualizada Receita à ser inserida no lugar da receita antiga.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void atualizar(String nome, Receita receitaAtualizada) {
		
		boolean nomeNaoNull = nome != null;
		boolean nomeNaoVazio = (nome.isEmpty()) == false; 
		
		if(nomeNaoNull && nomeNaoVazio) {
			Receita receitaAtualizar = buscaReceitaPeloNome(nome);
			List<Receita> receitas = this.lerReceitas();
			int indexReceitaAtualizar = receitas.indexOf(receitaAtualizar);
			receitas.set(indexReceitaAtualizar, receitaAtualizada);
			this.escreverReceitas(receitas);
		}
	}
	
	/**
	 * Exclui uma receita na lista receitas.
	 * 
	 *  @param nome Nome da receita à ser excluida.
	 */
	public void excluir(String nome){
		Receita receita = buscaReceitaPeloNome(nome);
		int indexReceita = this.receitas.indexOf(receita);
		this.receitas.remove(indexReceita);
	}
	
	// metodos de escrita json em arquivo
	private void escreverReceita(Receita receita) {
		Writer file = new FileWriter(this.jsonFile);
		List<Receita> receitas = lerReceitas();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		gson.toJson(receitas, file);
		gson.toJson(receita, file);
		file.close();
	}
	
	private void escreverReceitas(List<Receita> receitas) {
		Writer file = new FileWriter(this.jsonFile);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		gson.toJson(receitas, file);
		file.close();
	}
	
	@SuppressWarnings("unchecked")
	private List<Receita> lerReceitas() {
		Gson gson = new Gson();
		Reader file = new FileReader(this.jsonFile);
		List<Receita> receitas = gson.fromJson(file, List.class);
		file.close();
		return receitas;
	}
}
