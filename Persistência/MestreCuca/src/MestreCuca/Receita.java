package MestreCuca;

import java.util.ArrayList;
import java.util.List;

public class Receita {
	private int idReceita;
	private String nome;
	private List<Instrucao> instrucoes;
	private List<Ingrediente> ingredientes;
	
	/**
	 * Contrutor do objeto Receita.
	 * 
	 * @param nome Nome da receita.
	 */
	public Receita(String nome) {
		this.nome = nome;
		this.instrucoes = new ArrayList<>();
		this.ingredientes = new ArrayList<>();
	}
	
	// Metodos Get
	public int getIdReceita() {
		return idReceita;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public List<Instrucao> getInstrucoes() {
		return instrucoes;
	}
	
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}
	
	/**
	 * Calcula o preço total da receita.
	 * 
	 * @return double Preço total da receita.
	 */
	public double getPrecoTotal() {
		double precoTotal = 0;
		for(Ingrediente ingrediente : this.ingredientes) {
			precoTotal += ingrediente.getPreco();
		}
		return precoTotal;
	}
	
	// Metodos Set
	public void setIdReceita(int idReceita) {
		this.idReceita = idReceita;
	}
	
	public void adicionaIngrediente(Ingrediente ingrediente) {
		this.ingredientes.add(ingrediente);
	}

	
	
}
