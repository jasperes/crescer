package MestreCuca;

public class Instrucao {
	private String descricao;
	private int idInstrucao;
	private int idReceita;
	
	/**
	 * Construtor do objeto instrucao.
	 * 
	 * @param descricao Descricao da instrução.
	 */
	public Instrucao(String descricao) {
		this.descricao = descricao;
	}
	
	// Metodos Get
	public String getDescricao() {
		return descricao;
	}
	
	// Metodos Set
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
