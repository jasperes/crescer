package MestreCuca;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReceitaTestes {
	private double tolerancia = 0.0000009;
	
	@Test public void quandoCriarRecetaVerificarPrecoTotal() {
		Receita receita = new Receita("Brigadeiro");
		
		Ingrediente chocolate = new Ingrediente(UnidadeDeMedida.GRAMAS, "Chocolate", 100.0);
		Ingrediente leiteCondensado = new Ingrediente(UnidadeDeMedida.GRAMAS, "Leite condensado", 150.0);
		Ingrediente margarina = new Ingrediente(UnidadeDeMedida.GRAMAS, "Margarina", 25.0);
		
		chocolate.setPreco(1.50);
		leiteCondensado.setPreco(2.0);
		margarina.setPreco(2.0);
		
		receita.adicionaIngrediente(chocolate);
		receita.adicionaIngrediente(leiteCondensado);
		receita.adicionaIngrediente(margarina);
		
		double esperado = 5.5;
		double obtido = receita.getPrecoTotal();
		
		assertEquals(esperado, obtido, tolerancia);
	}


}
