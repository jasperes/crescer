package MestreCuca;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

public class GsonTests {

	@Test
	public void test() throws IOException {
		Writer file = new FileWriter("/home/jonathan/Documents/novoarquivo.txt");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		gson.toJson("nao", file);
		gson.toJson(" ou sim.", file);
		file.close();
	}
	
	@Test public void le() throws IOException {
		Gson gson = new Gson();
		FileReader file = new FileReader("/home/jonathan/Documents/arquivo.json");
		List<Receita> receitas = gson.fromJson(file, List.class);
		file.close();
	}

}
