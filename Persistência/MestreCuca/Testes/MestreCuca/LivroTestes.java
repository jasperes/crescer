package MestreCuca;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class LivroTestes {
	private double tolerancia = 0.0000009;

	// Livro()
	@Test public void quandoCriarLivroListaDeReceitasVazia() throws IOException {
		LivroReceitas livro = new Livro();
		
		List<Receita> esperado = new ArrayList<>();
		List<Receita> obtido = livro.getTodasReceitas();
		
		assertEquals(esperado, obtido);
	}
	
	// buscaReceitaPeloNome() - testes normais
	/*@Test public void quandoBuscarReceitaRetornarNull() {
		LivroReceitas livro = new Livro();
		
		Receita esperado = null;
		Receita obtido = livro.buscaReceitaPeloNome("nome");
		
		assertEquals(esperado, obtido);
	}*/
	
	@Test public void quandoBuscarReceitaRetornar() throws IOException {
		LivroReceitas livro = new Livro();
		Receita receita = new Receita("nome");
		livro.inserir(receita);
		
		Receita esperado = receita;
		Receita obtido = livro.buscaReceitaPeloNome("nome");
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void quandoBuscarReceitaQuandoTemVariasReceitasRetornar() throws IOException {
		LivroReceitas livro = new Livro();
		
		Receita torta = new Receita("torta");
		Receita bolo = new Receita("bolo");
		Receita bomba = new Receita("bomba");
		
		livro.inserir(torta);
		livro.inserir(bolo);
		livro.inserir(bomba);
		
		Receita esperado = bomba;
		Receita obtido = livro.buscaReceitaPeloNome("bomba");
		
		assertEquals(esperado, obtido);
	}
	
	// buscaReceitaPeloNome() - exceptions
	/*@Test(expected = NullPointerException.class)
	public void quandoBuscarNullOcorrerErro() {
		LivroReceitas livro = new Livro();
		
		try{
			livro.buscaReceitaPeloNome(null);
		}finally{
			
		}
	}*/
	
	@Test(expected = ReceitaIncompleta.class)
	public void quandoBuscarENaoEncontrarOcorrerErro() throws IOException {
		LivroReceitas livro = new Livro();
		
		try{
			livro.buscaReceitaPeloNome("");
		}finally{
			
		}
	}
	
	//getPrecoTotalReceitas()
	@Test public void quandoCriarLivroVerificarPrecoTotalDasReceitas() throws IOException {
		Livro livro = new Livro();
		
		// Brigadeiro
		Receita brigadeiro = new Receita("Brigadeiro");
		
		Ingrediente chocolate = new Ingrediente(UnidadeDeMedida.GRAMAS, "Chocolate", 100.0);
		Ingrediente leiteCondensado = new Ingrediente(UnidadeDeMedida.GRAMAS, "Leite condensado", 150.0);
		Ingrediente margarina = new Ingrediente(UnidadeDeMedida.GRAMAS, "Margarina", 25.0);
		
		chocolate.setPreco(1.50);
		leiteCondensado.setPreco(2.0);
		margarina.setPreco(2.0);
		
		brigadeiro.adicionaIngrediente(chocolate);
		brigadeiro.adicionaIngrediente(leiteCondensado);
		brigadeiro.adicionaIngrediente(margarina);
		
		// Algo Aleatório 1
		Receita aleatorio1 = new Receita("Brigadeiro");
		
		Ingrediente algo1 = new Ingrediente(UnidadeDeMedida.GRAMAS, "Chocolate", 100.0);
		
		algo1.setPreco(1.50);
		
		aleatorio1.adicionaIngrediente(algo1);
		
		// Algo Aleatório 2
		Receita aleatorio2 = new Receita("Brigadeiro");
		
		Ingrediente algo2 = new Ingrediente(UnidadeDeMedida.GRAMAS, "Chocolate", 100.0);
		
		algo2.setPreco(1.50);
		
		aleatorio2.adicionaIngrediente(algo2);
		
		// Adiciona Receitas ao Livro
		livro.inserir(brigadeiro);
		livro.inserir(aleatorio1);
		livro.inserir(aleatorio2);
		
		// variaveis de teste
		double esperado = 8.50;
		double obtido = livro.getPrecoTotalReceitas();
		
		assertEquals(esperado, obtido, tolerancia);
	}
	
	// getReceitasSemDeterminadosIngredientes()
	@Test public void verificarReceitasSemUmIngrediente() throws IOException {
		Livro livro = new Livro();
		
		Receita torta = new Receita("torta");
		Receita bolo = new Receita("bolo");
		
		Ingrediente chocolatePreto = new Ingrediente(UnidadeDeMedida.GRAMAS, "chocolate", 150.0);
		Ingrediente chocolateBranco = new Ingrediente(UnidadeDeMedida.GRAMAS, "chocolate branco", 150.0);
		
		torta.adicionaIngrediente(chocolatePreto);
		bolo.adicionaIngrediente(chocolateBranco);
		
		livro.inserir(torta);
		livro.inserir(bolo);
		
		List<Receita> esperado = new ArrayList<>();
		esperado.add(torta);
		
		List<Ingrediente> ingredientes = new ArrayList<>();
		ingredientes.add(chocolateBranco);
		List<Receita> obtido = livro.getReceitasSemDeterminadosIngredientes(ingredientes);
		
		assertEquals(esperado, obtido);
	}
	
	// getQuantidadeTotalIngredientes()
	@Test public void verificaQuantidadeTotalDeIngredientes() throws IOException {
		Livro livro = new Livro();
		
		Receita torta = new Receita("torta");
		Receita bolo = new Receita("bolo");
		
		Ingrediente chocolatePreto = new Ingrediente(UnidadeDeMedida.GRAMAS, "chocolate", 150.0);
		Ingrediente chocolateBranco = new Ingrediente(UnidadeDeMedida.GRAMAS, "chocolate branco", 150.0);
		Ingrediente margarina = new Ingrediente(UnidadeDeMedida.COLHER_DE_SOPA, "margarina", 2.0);
		
		torta.adicionaIngrediente(chocolatePreto);
		torta.adicionaIngrediente(chocolateBranco);
		bolo.adicionaIngrediente(chocolateBranco);
		bolo.adicionaIngrediente(margarina);
		
		livro.inserir(torta);
		livro.inserir(bolo);
		
		List<String> esperado = new ArrayList<>();
		esperado.add("300.0 Gramas de chocolate branco.");
		esperado.add("2.0 Colheres de sopa de margarina.");
		esperado.add("150.0 Gramas de chocolate.");
		
		List<String> obtido = livro.getQuantidadeTotalIngredientes();
		
		assertEquals(esperado, obtido);
	}

}
