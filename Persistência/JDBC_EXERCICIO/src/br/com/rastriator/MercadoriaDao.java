package br.com.rastriator;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MercadoriaDao {
	
	public List<Mercadoria> getTodasMercadorias() throws SQLException{
		Connection connection = ConnectionFactory.getConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery("Select * from mercadoria");
		
		List<Mercadoria> mercadorias = new ArrayList<>();
		while(result.next()) {
			Mercadoria mercadoria = new Mercadoria();
			mercadoria.setIdMercadoria(result.getInt(1));
			mercadoria.setDescricao(result.getString(2));
			mercadorias.add(mercadoria);
		}
		statement.close();
		return mercadorias;
	}
	
	public int getNumeroMercadorias() throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery("Select count(1) from mercadoria");
		int total = 0;
		while(result.next()){
			total = result.getInt(1);
		}
		statement.close();
		return total;
	}
	
	public void insert(Mercadoria mercadoria) throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = connection.prepareStatement(
				"insert into mercadoria("
				+ "idMercadoria, descricao)"
				+ "values (MERCADORIA_SEQ.NEXTVAL, ?);");
		statement.setString(1, mercadoria.getDescricao());
		statement.execute();
		connection.close();
	}
	
	public void update(Mercadoria mercadoria) throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = connection.prepareStatement(
				"update mercadoria"
				+ "set descricao = ?"
				+ "where idMercadoria = ?;");
		statement.setInt(1, mercadoria.getIdMercadoria());
		statement.setString(2, mercadoria.getDescricao());
		statement.execute();
		connection.close();
	}
	
	public void delete(Mercadoria mercadoria) throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = connection.prepareStatement(
				"delete from mercadoria"
				+ "where idMercadoria = ?;");
		statement.setInt(1, mercadoria.getIdMercadoria());
		statement.execute();
		connection.close();
	}
}