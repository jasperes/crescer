package br.com.rastriator;

public class Mercadoria {
	private int idMercadoria;
	private String descricao;
	
	public Mercadoria() {
		// TODO Auto-generated constructor stub
	}
	
	public void setIdMercadoria(int idMercadoria) {
		this.idMercadoria = idMercadoria;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public int getIdMercadoria() {
		return idMercadoria;
	}

	public String getDescricao() {
		return this.descricao;
	}
	
	
}
