package br.com.rastriator;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

public class MercadoriaDaoTest {
	@Test public void verificaNumeroDeMercadorias() throws SQLException {
		MercadoriaDao dao = new MercadoriaDao();
		int esperado = dao.getTodasMercadorias().size();
		int obtido = dao.getNumeroMercadorias();
		assertEquals(esperado, obtido);
	}
	
	
	
}
