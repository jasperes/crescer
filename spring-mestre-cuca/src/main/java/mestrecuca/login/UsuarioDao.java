package mestrecuca.login;

import org.springframework.jdbc.core.JdbcTemplate;

import mestrecuca.DataSourceFactory;

public class UsuarioDao {
	private JdbcTemplate jdbc;
	
	public UsuarioDao() {
		this.jdbc = new JdbcTemplate(DataSourceFactory.getDataSource());
	}
	
	/**
	 * Gera uma id para o Usuario.
	 * @return int Id nova do usuario.
	 */
	public int geraId(){
		String sql = "select USUARIO_SEQ.NEXTVAL";
		int idUsuario = this.jdbc.queryForObject(sql, Integer.class);
		return idUsuario;
	}
	
	/**
	 * Verifica se encontra algum usuario com os dados passados.
	 * @param usuario Usuario com dados à pesquisar.
	 * @return boolean Verdadeiro caso encontre, falso caso não encontre nenhum ou mais de um.
	 */
	public boolean dadosCorretos(Usuario usuario){
		StringBuilder builder = new StringBuilder();
		
		builder.append("select count(*) from usuario");
		builder.append(" where LOWER(login) = LOWER(?)");
		builder.append(" and senha = ?");
		
		String sql = builder.toString();
		
		String login = usuario.getLogin();
		String senha = usuario.getSenha();
		
		int encontrado = this.jdbc.queryForObject(sql, Integer.class, login, senha);
		return encontrado == 1 ? true : false;
	}
	
	/**
	 * Inseri um registro de Usuario no banco de dados.
	 * @param usuario Usuario à ser inserido no banco de dados.
	 * @return Usuario Objetor Usuario com valores cadastrados.
	 */
	public Usuario inserir(Usuario usuario) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("insert into usuario");
		builder.append(" (idUsuario, nome, login, senha)");
		builder.append(" values(?, ?, ?, ?)");
		
		String sql = builder.toString();
		
		usuario.setIdUsuario(this.geraId());
		
		int idUsuario = usuario.getIdUsuario();
		String nome = usuario.getNome();
		String login = usuario.getLogin();
		String senha = usuario.getSenha();
		
		jdbc.update(sql, idUsuario, nome, login, senha);
		
		return usuario;
	}
}