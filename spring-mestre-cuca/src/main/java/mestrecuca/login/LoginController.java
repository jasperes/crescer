package mestrecuca.login;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {
	
	private UsuarioDao dao = new UsuarioDao();

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String home(Model model){
		model.addAttribute("mensagem", "");
		return "login.html";
	}
	
	@RequestMapping(value = "/cadastro", method = RequestMethod.GET)
	public String cadastrar(Model model) {
		return "cadastro.html";
	}
	
	@RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
	public String cadastrarUsuario(Usuario usuario, Model model, HttpSession session) {
		if(usuario.validarDados()) {
			dao.inserir(usuario);
			model.addAttribute("mensagem", "Cadastrado com suscesso!");
			return "login.html";
		} else {
			return "cadastro.html";
		}
	}
	
	@RequestMapping(value = "/autentica", method = RequestMethod.POST)
	public String autentica(Usuario usuario, Model model, HttpSession session){
		if(dao.dadosCorretos(usuario)){
			//session.setAttribute("usuarioLogado", usuario);
			return "home.html";
		}else{
			model.addAttribute("mensagem", "Usuário ou senha inválidos");
			return "redirect:login.html";
		}
	}
}
