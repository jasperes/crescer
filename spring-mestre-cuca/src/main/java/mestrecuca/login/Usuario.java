package mestrecuca.login;

public class Usuario {

	private int idUsuario;
	private String nome;
	private String login;
	private String senha;
	
	// Methods GET
	public int getIdUsuario() {
		return idUsuario;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public String getSenha() {
		return this.senha;
	}
	
	public String getLogin() {
		return this.login;
	}
	
	// Methods Set
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	// Methods de validação
	public boolean validarDados() {
		boolean validado;
		validado = !(this.getLogin().isEmpty() || this.getNome().isEmpty() || this.getSenha().isEmpty());
		return validado;
	}
}
