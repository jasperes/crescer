package mestrecuca.ingrediente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import mestrecuca.DataSourceFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class IngredienteDao {
	private JdbcTemplate jdbc;
	
	public IngredienteDao() {
		this.jdbc = new JdbcTemplate(DataSourceFactory.getDataSource());
	}
	
	/**
	 * Gera uma id para o ingrediente
	 * @return int Novo id do ingrediente gerado.
	 */
	public int geraId() {
		String sql = "select INGREDIENTE_SEQ.NEXTVAL";
		return this.jdbc.queryForObject(sql, Integer.class);
	}
	
	/**
	 * Gera uma lista de ingredientes.
	 * @return List<Ingredientes> Ingredientes e suas Id/Nome.
	 */
	public List<Ingrediente> listaIngredientes() {
		String sql = "select idIngrediente, nome from ingrediente";
		return jdbc.query(sql, new RowMapper<Ingrediente>(){

			@Override
			public Ingrediente mapRow(ResultSet rs, int i) throws SQLException {
				Ingrediente ingrediente = new Ingrediente();
				ingrediente.setIdIngrediente(rs.getInt("idIngrediente"));
				ingrediente.setNome(rs.getString("nome"));
				return ingrediente;
			}
		});
	}
	
	public Ingrediente inseri(Ingrediente ingrediente) {
		StringBuilder builder = new StringBuilder();
		builder.append("insert into ingrediente");
		builder.append(" (idIngrediente, nome, unidade)");
		builder.append(" values(?,?,?)");
		String sql = builder.toString();
		
		ingrediente.setIdIngrediente(this.geraId());
		int idIngrediente = ingrediente.getIdIngrediente();
		String nome = ingrediente.getNome();
		String unidade = ingrediente.getUnidade().toString();
		
		this.jdbc.update(sql, idIngrediente, nome, unidade);
		
		return ingrediente;
	}
	
}
