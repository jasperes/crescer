package mestrecuca.ingrediente;

public enum Unidade {
	
	//NENHUMA_UNIDADE("",""),
	UNIDADE("Unidade", "Unidades"),
	GRAMAS("Grama", "Gramas"),
	MILIGRAMAS("Miligrama", "Miligramas"),
	KILOGRAMAS("Kilograma", "Kilogramas"),
	LITROS("Litro", "Litros"),
	COLHER_DE_SOPA("Colher de sopa", "Colheres de sopa"),
	DOSE("Dose", "Doses");

	private String descricaoSingular;
	private String descricaoPlural;
	
	private Unidade(String descricaoSingular, String descricaoPlural) {
		this.descricaoSingular = descricaoSingular;
		this.descricaoPlural = descricaoPlural;
	}
	
	public String getDescricao() {
		return this.descricaoSingular;
	}
	
	public String getDescricaoPlural() {
		return this.descricaoPlural;
	}
	
}
