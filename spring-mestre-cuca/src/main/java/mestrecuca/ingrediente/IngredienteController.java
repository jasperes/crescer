package mestrecuca.ingrediente;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class IngredienteController {
	private IngredienteDao dao = new IngredienteDao();

	@RequestMapping(value = "/ingredientes", method = RequestMethod.GET)
	public String home(Model model){
		model.addAttribute("ingredientes", dao.listaIngredientes());
		return "ingredientes.html";
	}
	
	@RequestMapping(value = "/cadastrar/ingrediente", method = RequestMethod.POST)
	public @ResponseBody String cadastrarIngrediente(@RequestBody Ingrediente ingrediente, Model model) {
		return Integer.toString(dao.inseri(ingrediente).getIdIngrediente());
	}
	
	@RequestMapping(value = "/getListaUnidade", method = RequestMethod.GET)
	public @ResponseBody Unidade[] retornarListaUnidade(Model model){
		return Unidade.values();
	}
	
	@RequestMapping(value = "/getListaIngredientes", method = RequestMethod.GET)
	public @ResponseBody List<Ingrediente> retornarListaIngredientes(Model model) {
		return dao.listaIngredientes();
	}
}
