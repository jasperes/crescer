package mestrecuca.receita;

import mestrecuca.ingrediente.Ingrediente;
import mestrecuca.ingrediente.Unidade;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ReceitaController {
	private ReceitaDao dao = new ReceitaDao();

	@RequestMapping(value = "/receitas", method = RequestMethod.GET)
	public String receitas(Model model) {
		model.addAttribute("receitas", this.dao.listaReceitas());
		return "receitas.html";
	}
	
	@RequestMapping(value = "/cadastrar/receita", method = RequestMethod.POST)
	public @ResponseBody String cadastrarIngrediente(@RequestBody Receita receita, Model model) {
		return Integer.toString(dao.inseri(receita).getIdReceita());
	}
}
