package mestrecuca.receita;

import java.util.List;

import mestrecuca.ingrediente.Ingrediente;

public class Receita {
	private int idReceita;
	private String nome;
	private String instrucao;
	private List<Ingrediente> ingredientes;
	
	// Methods SET
	public void setIdReceita(int idReceita) {
		this.idReceita = idReceita;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setInstrucao(String instrucao) {
		this.instrucao = instrucao;
	}
	
	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	// Methods GET
	public int getIdReceita() {
		return this.idReceita;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public String getInstrucao() {
		return this.instrucao;
	}
	
	public List<Ingrediente> getIngredientes() {
		return this.ingredientes;
	}

}
