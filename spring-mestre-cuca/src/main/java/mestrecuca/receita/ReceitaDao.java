package mestrecuca.receita;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import mestrecuca.DataSourceFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class ReceitaDao {
	private JdbcTemplate jdbc;
	
	public ReceitaDao() {
		this.jdbc = new JdbcTemplate(DataSourceFactory.getDataSource());
	}
	
	public int geraId() {
		String sql = "select RECEITA_SEQ.NEXTVAL";
		return this.jdbc.queryForObject(sql, Integer.class);
	}
	
	public List<Receita> listaReceitas(){
		String sql = "select idReceita, nome from receita";
		return this.jdbc.query(sql, new RowMapper<Receita>(){

			@Override
			public Receita mapRow(ResultSet rs, int i) throws SQLException {
				Receita receita = new Receita();
				receita.setIdReceita(rs.getInt("idReceita"));
				receita.setNome(rs.getString("nome"));
				return receita;
			}
		});
	}

	public Receita inseri(Receita receita) {
		StringBuilder builder = new StringBuilder();
		builder.append("insert into receita");
		builder.append(" (idReceita, nome)");
		builder.append(" values(?,?)");
		
		String sql = builder.toString();
		
		receita.setIdReceita(this.geraId());
		int idReceita = receita.getIdReceita();
		String nome = receita.getNome();
		
		this.jdbc.update(sql, idReceita, nome);
		
		return receita;
	}
}
