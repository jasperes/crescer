$(function(){
	
	// OnClick: Novo Ingrediente
	$("#novo-ingrediente").on("click", function(){
		// Altera o conteúdo central com o Form para cadastro do Ingrediente
		$("#mestre-cuca-conteudo").empty().append(
			$("<form class='pure-form' onsubmit='return false;'>").append(
				$("<h1 class='title'>").append("Cadastro de Ingrediente"),
				$("<input type='text' placeholder='Nome' name='nome'>").css({"width":"200px"}),
				$("<br>"),
				$("<select name='unidade'>").css({"width":"200px"}),
				$("<br>"),
				$("<input type='submit' value='Cadastrar' class='pure-button pure-button-primary' id='cadastra-receita'>")
			)
		).css({"text-align" : "center"});
		
		// Adicionar os itens ao combobox Unidade.
		$.get("/getListaUnidade", function(data){
			$.each(data, function(i,v){
				$("select[name='unidade']").append(
					$("<option value='"+v+"'>").append(v)
				);
			});
		});
		
		$("#cadastra-receita").on("click", function(){
			var inputNome = $("input[name='nome']");
			var inputUnidade = $("select[name='unidade']");
			var ingrediente = {};
			ingrediente.nome = inputNome.val();
			ingrediente.unidade = inputUnidade.val();
			$.ajax({
				url: '/cadastrar/ingrediente',
				type: 'POST',
				headers: { 
					'Accept': 'application/json',
					'Content-Type': 'application/json' 
				},
				data: JSON.stringify(ingrediente),
				success: function(data){
					$("#lista-ingredientes").append(
						$("<li>").append(
							$("<a href='#' id="+data+">").append(inputNome.val())
						)
					);
					$("input[name='nome']").val("");
					$("select[name='unidade']").val("UNIDADE");
				}
	        });
		});
		
	});
})