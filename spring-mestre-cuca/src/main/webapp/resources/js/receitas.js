$(function(){
	$("#nova-receita").on("click", function(){
		
		var listaIngredientes = [];
		var ingredientesCount;
		$.get("/getListaIngredientes", function(data) {
			$.each(data, function(i,v){
				listaIngredientes[i] = {"idIngrediente" : v["idIngrediente"], "nome" : v["nome"]};
				ingredientesCount = i;
			});
		});
		
		// Conteudo
		$("#mestre-cuca-conteudo").empty().append(
			$("<form class='pure-form' onsubmit='return false;'>").append(
				$("<h1 class='title'>").append("Cadastro de Receita"),
				$("<input type='text' name='nome' placeholder='Nome'>"),
				$("<br>"),
				$("<textarea name='instrucao' placeholder='Instruções da receita'>").css(
					{"width":"400px", "height":"200px"}
				),
				$("<br>"),
				$("<input type='submit' id='cadastra-receita' class='pure-button pure-button-primary' value='Cadastrar'>"),
				$("<br>"),
				$("<span>")
			)
		).css({"text-align":"center"});
		
		// Lateral
		$("#mestre-cuca-lateral").empty().append(
			$("<div class='pure-menu pure-menu-open'>").append(
				$("<ul id='lista-ingredientes'>").append(
					$("<a class='pure-menu-heading'>").append("Ingredientes")
				)
			)
		);
		
		// Appenda lista de ingredientes
		$.each(listaIngredientes, function(i,v) {
			$("#lista-ingredientes").append(
				$("<li>").append(
					$("<a>").append(
						$("<input type='checkbox' class='ingrediente' name='idIngrediente' value='"+v+"'>")
					).append(
							v["nome"]
					)
				)
			);
		});
		
		// Salvar receita
		$("#cadastra-receita").on("click", function(){
			var ingredientes = [];
			var count = 0;
			$.each($(".ingrediente"), function(i,v){
				ingredientes[count] = {"idIngrediente" :$(v).attr("value")};
				count++;
			})
			console.log(ingredientes);
		})
		
	});
		
});