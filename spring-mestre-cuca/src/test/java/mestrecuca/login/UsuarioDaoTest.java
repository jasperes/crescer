package mestrecuca.login;

import static org.junit.Assert.*;

import org.junit.Test;

public class UsuarioDaoTest {

	@Test public void procuraUsuarioEncontrando() {
		Usuario usuario = new Usuario();
		usuario.setLogin("Admin");
		usuario.setSenha("43690");
		boolean encontrado = new UsuarioDao().dadosCorretos(usuario);
		assertTrue(encontrado);
	}
	
	@Test public void inseriUmaNovaId() {
		int id = new UsuarioDao().geraId();
		boolean gerou = id > 0;
		assertTrue(gerou);
	}

}
