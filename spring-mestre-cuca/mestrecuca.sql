create table Usuario (
  IdUsuario		integer			not null,
  Nome			varchar2(50)	not null,
  Login			varchar2(50)	not null 	unique,
  Senha			varchar2(20),
  constraint	PK_Usuario		primary key	(IdUsuario)
);

create sequence USUARIO_SEQ;

insert into Usuario(idUsuario, nome, login, senha) values(USUARIO_SEQ.NEXTVAL, 'Administrador', 'Admin', '43690');

create table Receita (
  IdReceita		integer			not null,
  Nome			varchar2(30)	not null,
  constraint 	PK_Receita		primary key	(IdReceita)
);

create sequence RECEITA_SEQ;

create table Ingrediente (
  IdIngrediente	integer			not null,
  Nome			varchar2(30)		not null,
  Unidade		varchar2(30)	not null,
  constraint	PK_Ingrediente	primary key	(IdIngrediente)
);

create sequence INGREDIENTE_SEQ;

create table Receita_Ingrediente (
  IdReceitaIngrediente	integer		not null,
  IdReceita				integer		not null,
  IdIngrediente			integer		not null,
  Quantidade			number(8,2)	not null,
  constraint	PK_ReceitaIngrediente	primary key	(IdReceitaIngrediente),
  constraint	FK_ReceitaIngrediente_Receita	foreign key	(IdReceita)
  	references	Receita	(IdReceita),
  constraint	FK_ReceitaIngrediente_Ingrediente	foreign key	(IdIngrediente)
  	references	Ingrediente	(IdIngrediente)
);

create sequence RECEITAINGREDIENTE_SEQ;

create table instrucao (
  IdInstrucao	integer		not null,
  instrucao		clob		not null,
  constraint	PK_Instrucao	primary key	(IdInstrucao)
);

create sequence INSTRUCAO_SEQ;