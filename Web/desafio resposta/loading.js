document.addEventListener('DOMContentLoaded', initPage, false);

INTERVALO_MILISEGUNDOS = 1; // equivale à um décimo de segundo

function initPage() {
	// obtendo referencia do botao, para referenciar o evento de clique.
	// equivale à <button onclick="">, mas não interfere no HTML.
	var button = document.getElementById('btnPlay');
	button.onclick = function() {

		var progress = document.getElementsByClassName('current-progress')[0]
		, contadorDeProgresso = 0;

		var incrementaProgresso = setInterval(function() {

			if (contadorDeProgresso === 100) {
				clearInterval(incrementaProgresso);
			} else {
				// primeiro incrementando o progresso e depois concatenando com '%'. Ex: '17%';
				progress.style.width = ++contadorDeProgresso + '%';
			}

		}, INTERVALO_MILISEGUNDOS);
	}
}