var img = document.getElementById("logo");
img.removeAttribute("href");
img.onclick  = function() {
  var solucoes = document.getElementById("solucoes");

  var c0 = solucoes.children[0];
  var c1 = solucoes.children[1];
  var c2 = solucoes.children[2];
  var c3 = solucoes.children[3];

  solucoes.removeChild(c0);
  solucoes.removeChild(c1);
  solucoes.removeChild(c2);
  solucoes.removeChild(c3);

  solucoes.appendChild(c3);
  solucoes.appendChild(c2);
  solucoes.appendChild(c1);
  solucoes.appendChild(c0);
};
