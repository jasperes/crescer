
connect crescer/CRESCER;

-- LAB1
Create table Cidade 
(
  IDCidade  integer     not null,
  Nome      varchar2(30) not null,
  UF        varchar2(2)  not null,
    constraint PK_Cidade primary key (IDCidade)
);

Create table Associado 
(
  IDAssociado    integer     not null,
  Nome           varchar2(50) not null,
  DataNascimento date    not null,
  Sexo           char(1)     not null,
  CPF            varchar2(11) ,
  Endereco       varchar2(35),Grant CONNECT, RESOURCE, CREATE VIEW, CREATE SEQUENCE to Crescer14;
file:/home/jonathan/SVN/jonathan-peres/Banco de dados/System.sql
file:/home/jonathan/Codes/SVN-CRESCER/jonathan-peres/Banco de dados/System.sql

  Bairro         varchar2(25),
  Complemento    varchar2(20),
  IDCidade       integer,
    constraint PK_Associado primary key (IDAssociado),
    constraint FK_Associado foreign key (IDCidade)
    references Cidade (IDCidade)
);

-- LAB2
insert into Cidade (IDCidade, Nome, UF)
   values (1, 'Sao Leopoldo', 'RS');
insert into Cidade (IDCidade, Nome, UF)
   values (2, 'Porto Alegre', 'RS');
insert into Cidade (IDCidade, Nome, UF)
   values (3, 'Parobe', 'RS');
insert into Cidade (IDCidade, Nome, UF)
   values (4, 'Taquara', 'RS');
insert into Cidade (IDCidade, Nome, UF)
   values (105, 'S�o Paulo', 'SP');
insert into Cidade (IDCidade, Nome, UF)
   values (106, 'Guarulhos', 'SP');
insert into Cidade (IDCidade, Nome, UF)
   values (27, 'Rio de Janeiro', 'RJ');
insert into Cidade (IDCidade, Nome, UF)
   values (108, 'Brasilia', 'DF');
insert into Cidade (IDCidade, Nome, UF)
   values (109, 'Belo Horizonte', 'MG');
insert into Cidade (IDCidade, Nome, UF)
   values (110, 'Uberl�ndia', 'MG');
insert into Cidade (IDCidade, Nome, UF)
   values (5, 'Cidade de Alagoas', 'AL');
insert into Cidade (IDCidade, Nome, UF)
   values (6, 'Maceio', 'AL');
insert into Cidade (IDCidade, Nome, UF)
   values (7, 'Itu', 'SP');
insert into Cidade (IDCidade, Nome, UF)
   values (8, 'Campinas', 'SP');
insert into Cidade (IDCidade, Nome, UF)
   values (9, 'Guaratingueta', 'SP');
insert into Cidade (IDCidade, Nome, UF)
   values (10, 'Santa Barbara', 'MG');
insert into Cidade (IDCidade, Nome, UF)
   values (11, 'Campos dos Goitacases', 'RJ');
insert into Cidade (IDCidade, Nome, UF)
   values (12, 'Sao Gabriel', 'RS');
insert into Cidade (IDCidade, Nome, UF)
   values (13, 'Brasopolis', 'MG');
insert into Cidade (IDCidade, Nome, UF)
   values (14, 'Cristina', 'MG');
insert into Cidade (IDCidade, Nome, UF)
   values (15, 'Umbuzeiro', 'PB');
insert into Cidade (IDCidade, Nome, UF)
   values (16, 'Vicosa', 'MG');
insert into Cidade (IDCidade, Nome, UF)
   values (17, 'Macae', 'RJ');
insert into Cidade (IDCidade, Nome, UF)
   values (18, 'Sao Luis', 'MA');
insert into Cidade (IDCidade, Nome, UF)
   values (19, 'Cuiaba', 'MT');
insert into Cidade (IDCidade, Nome, UF)
   values (20, 'Sao Borja', 'RS');
insert into Cidade (IDCidade, Nome, UF)
   values (21, 'Natal', 'RN');
insert into Cidade (IDCidade, Nome, UF)
   values (22, 'Diamantina', 'MG');
insert into Cidade (IDCidade, Nome, UF)
   values (23, 'Campo Grande', 'MS');
insert into Cidade (IDCidade, Nome, UF)
   values (24, 'Fortaleza', 'CE');
insert into Cidade (IDCidade, Nome, UF)
   values (25, 'Bage', 'RS');
insert into Cidade (IDCidade, Nome, UF)
   values (26, 'Bento Goncalves', 'RS');
insert into Cidade (IDCidade, Nome, UF)
   values (28, 'Pinheiro', 'MA');
insert into Cidade (IDCidade, Nome, UF)
   values (29, 'Salvador', 'BA');
insert into Cidade (IDCidade, Nome, UF)
   values (30, 'Garanhuns', 'PE');
Insert into Cidade (IDCidade, Nome, UF)
   values (31, 'S�o Paulo', 'SP');    
Insert into Cidade (IDCidade, Nome, UF)
   values (32, 'Guarulhos', 'SP');    
Insert into Cidade (IDCidade, Nome, UF)
   values (33, 'Rio de Janeiro', 'RJ');    
Insert into Cidade (IDCidade, Nome, UF)
   values (34, 'Brasilia', 'DF');    
Insert into Cidade (IDCidade, Nome, UF)
   values (35, 'Belo Horizonte', 'MG');        
Insert into Cidade (IDCidade, Nome, UF)
   values  (36, 'Uberl�ndia', 'MG');      
Insert into Cidade (IDCidade, Nome, UF)
   values  (37, 'Santana do Livramento', 'RS');
Insert into Cidade (IDCidade, Nome, UF)
   values  (38, 'Santa Maria', 'RS');
Insert into Cidade (IDCidade, Nome, UF)
   values  (39, 'Santa Rosa', 'RS');
Insert into Cidade (IDCidade, Nome, UF)
   values  (40, 'S�o Francisco de Paula', 'RS');    

-- ASSOCIADOS...
insert into Associado 
      (IDAssociado, 
       Nome, 
       DataNascimento, 
       Sexo)
  values 
      (1, 
       'Ana Paula Padrao', 
       TO_DATE('10/05/1980', 'DD/MM/YYYY'), 
       'F' );
--
insert into Associado 
      (IDAssociado, 
       Nome, 
       DataNascimento, 
       Sexo)
  values  
      (2, 
       'Joao Soares', 
       TO_DATE('20/02/1980', 'DD/MM/YYYY'), 
       'M' );

-- NO BANCO DE DADOS ORACLE � OBRIGAT�RIO O USO DO COMMIT AP�S QUALQUER TRANSA��O, MESMO QUE N�O TENHA EXECUTADO O "BEGIN TRANSACTION"
COMMIT;

create table copiaCidade as select * from Cidade;

select * from copiaCidade where IDCidade=1;

update copiaCidade set nome='Novo nome' where IDCidade=1;

delete from copiaCidade where IDCidade=1;

ROLLBACK;

Insert into Associado (IDAssociado, Nome, DataNascimento, Sexo, CPF, Endereco, Bairro, COmplemento, IDCIdade)
Values(
  5,
  'Jo�o da Rosa',
  TO_DATE('1980-01-01', 'yyyy-mm-dd'),
  'M',
  '03316655069',
  'R. Alvorada, n� 111',
  'Dona Augusta',
  '',
  '1'
);

Create table CidadeAux as
Select * from Cidade;

Truncate table CidadeAux;

Insert into CidadeAux (IDCidade, Nome, UF)
Select IDCidade, Nome, UF from Cidade;

COMMIT;

-- LAB3
-- Table "Emp" ... --
create table Empregado (
 IDEmpregado    integer not null
,NomeEmpregado  varchar2(30) not null
,Cargo          varchar2(15) not null
,IDGerente      integer null
,DataAdmissao   date not null
,Salario        number(7,2) not null
,Comissao       number(7,2)
,IDDepartamento integer
);

insert into Empregado values (7369 ,'SMITH', 'Atendente', '7902', to_date('1980/12/17', 'YYYY/MM/DD'),  800, null, 20);
insert into Empregado values (7499 ,'ALLEN', 'Vendedor', '7698', to_date('1981/02/20', 'YYYY/MM/DD'),  1600, 300, 30);
insert into Empregado values (7521 ,'WARD', 'Vendedor', '7698', to_date('1981/02/22', 'YYYY/MM/DD'),  1250, 500, 30);
insert into Empregado values (7566 ,'JONES', 'Gerente', '7839', to_date('1981/04/02', 'YYYY/MM/DD'),  2975, null, 20);
insert into Empregado values (7654 ,'MARTIN', 'Vendedor', '7698', to_date('1981/09/28', 'YYYY/MM/DD'),  1250, 1400, 30);
insert into Empregado values (7698 ,'BLAKE', 'Gerente', '7839', to_date('1981/05/01', 'YYYY/MM/DD'),  2850, null, 30);
insert into Empregado values (7782 ,'CLARK', 'Gerente', '7839', to_date('1981/06/09', 'YYYY/MM/DD'),  2450, null, 10);
insert into Empregado values (7788 ,'SCOTT', 'Analista', '7566', to_date('1982/12/09', 'YYYY/MM/DD'),  3000, null, 20);
insert into Empregado values (7839 ,'KING', 'Presidente', null, to_date('1981/11/17', 'YYYY/MM/DD'),  5000, null, null);
insert into Empregado values (7844 ,'TURNER', 'Vendedor', '7698', to_date('1981/09/08', 'YYYY/MM/DD'),  1500, 0, 30);
insert into Empregado values (7876 ,'ADAMS', 'Atendente', '7788', to_date('1983/01/12', 'YYYY/MM/DD'),  1100, null, 20);
insert into Empregado values (7900 ,'JAMES', 'Atendente', '7698', to_date('1981/12/03', 'YYYY/MM/DD'),  950, null, 30);
insert into Empregado values (7902 ,'FORD', 'Analista', '7566', to_date('1981/12/03', 'YYYY/MM/DD'),  3000, null, 20);
insert into Empregado values (7934 ,'MILLER', 'Atendente', '7782', to_date('1982/01/23', 'YYYY/MM/DD'),  1300, null, 10);
insert into Empregado values (7940 ,'ANDREW', 'Atendente', '7782', to_date('1988/01/20', 'YYYY/MM/DD'),  1150, null, null);

create table Departamento (
 IDDepartamento   integer not null
,NomeDepartamento varchar2(30)
,Localizacao      varchar2(25)
);

insert into Departamento values (10,'Contabilidade', 'SAO PAULO');
insert into Departamento values (20,'Pesquisa', 'SAO LEOPOLDO');
insert into Departamento values (30,'Vendas', 'SAO PAULO');
insert into Departamento values (40,'Opera��es', 'RECIFE');
insert into Departamento values (1, 'Presid�ncia', 'RIBEIRAO PRETO');
insert into Departamento values (60, 'Tecnologia', 'PORTO ALEGRE');

---------
ALTER TABLE Departamento ADD  CONSTRAINT PK_departamento 
  PRIMARY KEY  
  (IDDepartamento);

ALTER TABLE Empregado ADD  CONSTRAINT PK_Empregado
  PRIMARY KEY  
  (IDEmpregado);

ALTER TABLE Empregado ADD CONSTRAINT FK_Empregado_Departamento
  FOREIGN KEY (IDDepartamento) REFERENCES Departamento (IDDepartamento);

CREATE INDEX IX_Empregado_Departamento ON Empregado (IDDepartamento);

Create view VW_Empregados as
  Select IDEmpregado ID,
       NomeEmpregado Nome,
       Salario  SalarioMensal,
       (Salario * 12) SalarioAnual
  From Empregado;
  
Select * from VW_Empregados;

-- Exercicios
-- 1
Select NomeEmpregado from Empregado order by DataAdmissao;

-- 2
Select NomeEmpregado as Nome,
       (Salario * 12) as Salario_Anual
  From Empregado
  Where Cargo = 'Atendente'
  Or Salario < 18500.00/12;
  
-- 3
Select IDCidade as ID from Cidade where lower(Nome) = 'uberl�ndia';

-- 4
Select IDCidade, Nome from Cidade where UF = 'RS';

-- Exercicio
-- 1
Select SUBSTR(Nome, 1, (INSTR(Nome, ' '))) from associado;

-- 2
Select Nome,
       TRUNC(MONTHS_BETWEEN(sysdate, DataNascimento)/12) Idade
  From Associado;
  
-- 3 
select * from empregado

Select NomeEmpregado,
       TRUNC(MONTHS_BETWEEN(TO_DATE('31/12/2000', 'DD/MM/YYYY'), DataAdmissao)) Diferenca
  From Empregado
  Where DataAdmissao BETWEEN TO_DATE('01/05/1980', 'DD/MM/YYYY') and TO_DATE('20/01/1982', 'DD/MM/YYYY');
  
-- 4
-- minhas resposta
Select Max(Cargo),
       Count(1) Empregados
  From Empregado;
 
--resposta correta
Create View cw_cargos  as
Select Cargo,
       COUNT(1) Total_Empregados
  From Empregao
  Group by Cargo
  Order by Total_Empregados desc;

Select * from vw_cargos where rownum = 1;

-- 5
--Select NomeEmpregado,
--       Max(LENGTH(NomeEmpregado))
--  From Empregado;

Select Nome, LENGTH(Nome)
  From Associado
  Where LENGTH(Nome) = (
    Select MAX(LENGTH(Nome))
    From Associado);

-- 6
Select Nome,
       DataNascimento,
       ADD_MONTHS(DataNascimento, (50*12)) Cinquenta_Anos,
       TO_CHAR(ADD_MONTHS(DataNascimento, (50*12)), 'DAY') Dia_Da_Semana
  From Associado;

-- 7
Select UF,
       COUNT(*) Total_Cidades
  From Cidade
  Group by UF
  Order by Total_Cidades desc;

-- 8
Select Nome,
       UF,
       COUNT(1),
  From Cidade
  Group by Nome, UF
  Having COUNT(1) > 1;

-- 9
Select NVL(MAX(IDAssociado), 0) + 1 Proximo_ID
  From Associado;

-- 10
Truncate Table CidadeAux;

Insert into CidadeAux(IDCidade, Nome, UF)
Select MIN(IDCidade), Nome, UF
  From Cidade
  Group by Nome, UF;

-- 11
Update Cidade
  Set Nome = CONCAT('*', Nome)
  Where Nome in (
    Select Nome
    From (
        Select Nome, UF
        From Cidade
        Group by Nome, UF
        Having COUNT(*) > 1
    )
  );
  
COMMIT;

-- 12
Select Nome,
  DECODE(Sexo, 'M', 'Masculino', 'F', 'Feminino', 'Outro') Sexo
  From Associado;

-- 13
Select NomeEmpregado,
  Salario,
  Case when Salario <= 1164.00 then '0%'
       when Salario <= 2326.00 then '15%'
       else '27,5%'
  End Desconto
  From Empregado;

-- 14
  Delete From Cidade
  Where Nome in (
    Select distinct Nome
    From (
        Select Nome, UF
        From Cidade
        Group by Nome, UF
        Having COUNT(*) > 1
    )
  );
  
  rollback
-- 15


-- exercicios join & sub-querys
update associado
set idcidade = 1
where idassociado = 1
;
update associado
set idcidade = 32
where idassociado = 3
;


-- 1

Select e.NomeEmpregado,
       d.NomeDepartamento
  From Empregado e
  Inner join Departamento d
  On e.IDDepartamento = d.IDDepartamento;

-- 2
Select a.Nome,
       NVL(c.Nome, 'Não tem cidade')
  From Associado a
  Left join Cidade c
  On a.IDCidade = c.IDCidade;

-- 3
Select c.UF,
       count(1)
  From Cidade c
  Where not exists (select 1 from associado a where c.idcidade = a.idcidade)
  Group by c.UF;
  
-- 4
Select a.Nome,
       c.Nome,
       Case
         When c.UF in ('RS', 'SC', 'PR') then '***'
         else Null
       End
  From Associado a
  Inner join Cidade c
    On a.IDCidade = c.IDCidade;
    
-- 5

Select eE.NomeEmpregado Empregado,
       dE.NomeDepartamento Departamento_Empregado,
       eG.NomeEmpregado Gerente,
       dG.NomeDepartamento Departamento_Gerente
  From Empregado eE
  Inner join Empregado eG
    On eE.IDGerente = eG.IDEmpregado
  Inner join Departamento dG 
    On eG.IDDepartamento = dG.IDDepartamento
  Inner join Departamento dE
    On eE.IDDepartamento = dE.IDDepartamento;
    
-- 6

Create table EmpregadoCopia as
Select * from Empregado;

Update EmpregadoCopia
  Set Salario = Salario + ((Salario/100)*14.5)
  Where IDDepartamento in (
    Select IDDepartamento From Departamento
      Where Localizacao = 'SAO PAULO');
      
-- 7
Select SUM(e.Salario) Total_Antes,
       SUM(eN.Salario) Total_Novo,
        SUM(eN.Salario) - SUM(e.Salario)
  From EmpregadoCopia eN
  Inner join Empregado e
    On eN.IDEmpregado = e.IDEmpregado;
      
-- 8
  
Select NomeDepartamento
  From Departamento
  Where IDDepartamento in (
    Select IDDepartamento 
    From Empregado 
    Where Salario = (
      Select MAX(Salario)
      From Empregado
      Where IDDepartamento is not null
    )
  )
;

-- 9

Select a.Nome Nome,
       c.Nome Cidade
  From Associado a
  Inner Join Cidade c
    On a.IDCidade = c.IDCidade
  Union all
Select e.NomeEmpregado Nome,
       d.Localizacao as Cidade
  From Empregado e
  Inner join Departamento d
    On e.IDDepartamento = d.IDDepartamento;

-- 10

Select IDCidade, Nome, UF
  From Cidade
  Where exists (
    Select 1
    From Associado
    Where Associado.IDCidade = Cidade.IDCidade
  );
  
