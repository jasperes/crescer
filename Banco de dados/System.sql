Create tablespace CRESCER_DAT
  datafile '/lib/oracle/oradata/XE/oradatacrescerdat01.dbf'
  size 100m
  autoextend on
  next 100m
  maxsize 2048m;
  
  
Create user CRESCER identified by CRESCER
default tablespace CRESCER_DAT;

grant connect, resource, create view to CRESCER;

-- importar banco
Create user Crescer14 identified by CRESCER14 default tablespace crescer_dat;
Grant CONNECT, RESOURCE, CREATE VIEW, CREATE SEQUENCE to Crescer14;

-- Select objetos após a criação:
Select * from CAT;

-- Comando de importação:
imp system/43690@XE file=Crescer14.dmp FromUser=CRESCER14 ToUser=CRESCER14 Log=importação_crescer14.log

-- Select objetos após importação:
Select * from CAT;

