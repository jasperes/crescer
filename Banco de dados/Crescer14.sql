-- Exercicios
-- 1
Select COUNT(distinct Situacao) From Produto;

-- 2
Select * From Cliente Where UPPER(Nome) like '%LTDA%' Or UPPER(RazaoSocial) like '%LTDA%';

-- 3

Insert into Produto
  (
   IDProduto,
   Nome,
   DataCadastro,
   PrecoCusto,
   PrecoVenda,
   Situacao
  )
  Values
  (
   (Select MAX(IDProduto) From Produto) + 1,
   'Galocha Maragato',
   TO_DATE(SYSDATE, 'DD/MM/YYYY'),
   35.67,
   77.95,
   'A'
  );

COMMIT;

-- 4

Select Prd.Nome
  From Produto Prd
  Where not exists (
    Select 1
    From PedidoItem PdI
    Where Prd.IDProduto = PdI.IDProduto
    );
    
-- 5

Create view VW_ClienteEstado 
Select cc.UF Estado,
           COUNT(c.IDCidade) Quantidade
      From Cliente c
      Inner join Cidade cc
        On c.IDCidade = cc.IDCidade
      Group by cc.UF
      Order by Quantidade;

Select Estado,
       Quantidade
  From (
    Select cc.UF Estado,
           COUNT(c.IDCidade) Quantidade
      From Cliente c
      Inner join Cidade cc
        On c.IDCidade = cc.IDCidade
      Group by cc.UF
      Order by Quantidade desc
  ) 
  Where rownum = 1
Union all
Select Estado,
       Quantidade
  From (
    Select cc.UF Estado, COUNT(c.IDCidade) Quantidade
      From Cliente c
      Inner join Cidade cc
        On c.IDCidade = cc.IDCidade
      Group by cc.UF
      Order by Quantidade
  )
  Where rownum = 1;
  
-- 6

Select COUNT(distinct Nome)  
  From Cidade
  Where IDCidade in (Select IDCidade
                    From Cliente
                    Where exists (Select 1
                                  From Pedido
                                  Where Pedido.IDCliente = Cliente.IDCliente)
                    );

Select Nome
  From Cidade
  Where IDCidade in (Select distinct IDCidade
                    From Cliente
                    Where exists (Select 1
                                  From Pedido
                                  Where Pedido.IDCliente = Cliente.IDCliente)
                    );
  
-- 7

Select * From Produto Where rownum = 1;

Select * From Material Where rownum = 1;

Select * From ProdutoMaterial Where rownum = 1;

