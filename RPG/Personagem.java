import java.util.ArrayList;

/**
 * Define um Personagem no sistema.
 * 
 * @author CWI Software
 * @version 15-09-2014 15:36
 */
public abstract class Personagem
{
    protected String nome;
    protected double vida;
    protected double magia;
    protected int experiencia;
    protected Status status;
    protected ArrayList<ItemDoInventario> itensDoInventario;
    private static int contadorDePersonagem;
    
    /**
     * Construtor do objeto Personagem.
     */
    public Personagem() {
        this.contadorDePersonagem++;
        this.nome = "Sem Nome";
        this.vida = 100;
        this.magia = 100;
        this.status = Status.VIVO;
        this.itensDoInventario = new ArrayList<>();
    }
    
    // Method Get
    public String getNome() {
        return this.nome;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public double getMagia() {
        return this.magia;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    /**
     * @return String Descrições dos itens.
     */
    public String getDescricoesItens() {
        StringBuilder itens = new StringBuilder();
        
        /*for(int i=0; i < this.itensDoInventario.size(); i++) {
            if(i>0 && i != this.itensDoInventario.size()){itens.append(", ");}
            itens.append((this.itensDoInventario.get(i)).getDescricao());
        }*/
        
        // Para cada item nos itens do inventário:
        // Adiciona o item à string.
        for(ItemDoInventario item : this.itensDoInventario) {
            itens.append(!itens.toString().isEmpty() ? ", " : "");
            /*if(itens.toString().isEmpty() == false) {
                itens.append(", ");
            }*/
            itens.append(item.getDescricao());
        }
        
        return itens.toString();
    }
    
    /**
     * @return ItemDoInventario Retorna o item com maior quantidade dos itens.
     */
    public ItemDoInventario getItemComMaiorQuantidade() {
        if(!this.itensDoInventario.isEmpty()){
            ItemDoInventario maiorItem = this.itensDoInventario.get(0);
        
            for(ItemDoInventario item : this.itensDoInventario) {
                // maiorItem = item.getQuantidade > maiorItem.getQuantidade
                if(item.getQuantidade() > maiorItem.getQuantidade()) {
                    maiorItem = item;
                }
            }
            return maiorItem;
        }else{
            return null;
        }
        
    }
    
    public static int getContadorPersonagem() {
        return Personagem.contadorDePersonagem;
    }
    
    // Methods Set
    public void setStatus(Status status) {
        this.status = status;
    }
    
    protected void setExperiencia(int xp) {
        this.experiencia = xp;
    }
    
    public static void resetContadorPersonagem() {
        Personagem.contadorDePersonagem = 0;
    }
    
    // Methods
    /**
     * Altera vida do objeto.
     * Se vida zerada, altera Status para MORTO.
     * Se vida acima de zero, altera Status para VIVO.
     * 
     * @param Quantidade Quantidade à ser alterada na vida.
     */
    protected void alteraVida(double quantidade) {
        this.vida = (int)(this.vida + quantidade) <= 0 ? 0 : this.vida + quantidade;
        this.status = (int)this.vida <= 0 ? Status.MORTO : this.status;
    }
    
    /**
     * Adiciona um item ao inventario.
     * 
     * @param Item Item à ser incluido no inventario.
     */
    public void adicionarItem(ItemDoInventario item) {
        if(item.getQuantidade() > 0) {
                this.itensDoInventario.add(item);
        }
    }
    
    /**
     * Remove um item do inventario.
     * 
     * @param Item Item à ser removido do inventario.
     * @return Boolean Se removido, retorna True. Do contrário, retorna False.
     */
    public boolean perderItem(ItemDoInventario item) {
        if(this.itensDoInventario.contains(item)){
            this.itensDoInventario.remove(this.itensDoInventario.indexOf(item));
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Ordena os itens do inventário por ordem de quantidade.
     * 
     * Essa ordenação é uma bosta, algoritimo bem cagado.
     * 
     * Se precisa disso, vai fazer outro descente. GOOGLE IT!
     */
    public void ordenaItens() {
        ArrayList<ItemDoInventario> itensPosicionados = this.itensDoInventario;
        
        /*
         ArrayList<ItemDoInventario> itensPosicionados = new ArrayList<>(this.itensDoInventario.size());
         for(ItemDoInventario c : this.itensDoInventario) {
            itensPosicionados.add(c);
        }*/
        
        for(ItemDoInventario item : this.itensDoInventario) {
            for(ItemDoInventario itemPosicionado : itensPosicionados){
                
                // Se o item e o item posicionado forem o mesmo objeto, para loop e verifica próximo item.
                if(item == itemPosicionado) {
                    break;
                }
                
                // Se o item for menor do que o item posicionado
                // o item terá que ficar no lugar do item posicionado
                // e o item posicionado na posição abaixo
                if(item.getQuantidade() < itemPosicionado.getQuantidade()) {
                    
                    int indexPosicionado = itensPosicionados.indexOf(itemPosicionado);
                    int indexItem = itensPosicionados.indexOf(item);
                     
                    // move os itens na posição abaixo dele para aqueles entre o item e o item posicionado
                    for(int i = indexItem; i > indexPosicionado; i--) {
                        itensPosicionados.set(i, itensPosicionados.get(i-1));
                    }
                    
                    // coloca o item posicionado na posição abaixo e o item no lugar dele
                    itensPosicionados.set(indexPosicionado, item);
                    itensPosicionados.set(indexPosicionado+1, itemPosicionado);
                    
                    break;
                }
            }
        }
        
        this.itensDoInventario = itensPosicionados;
    }
    
    // Contrato
    public abstract void atacarPersonagem(Personagem personagem);
}
