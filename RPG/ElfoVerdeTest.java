

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Cenários para teste do objeto ElfoVerde.
 *
 * @author  CWI Software
 * @version 15-09-2014 16:15
 */
public class ElfoVerdeTest
{
    @Test public void quantoAtirarFlechaAumentarExperienciaEmDois() {
        ElfoVerde elfoVerde = new ElfoVerde();
        elfoVerde.atirarFlecha(new Orc());
        
        int obtido = elfoVerde.getExperiencia();
        assertEquals(2, obtido);
    }
    
    @Test public void quandoAtirarTresFlechasAumentarExperienciaEmSeis() {
        ElfoVerde elfoVerde = new ElfoVerde();
        
        elfoVerde.atirarFlecha(new Orc());
        elfoVerde.atirarFlecha(new Orc());
        elfoVerde.atirarFlecha(new Orc());
        
        int obtido = elfoVerde.getExperiencia();
        assertEquals(6, obtido);
    }
    
    @Test public void quandoReceberItemRecusarItemQueNaoPodeUsar() {
        ElfoVerde elfoVerde = new ElfoVerde();
        
        ItemDoInventario item = new ItemDoInventario("Espada sem fio", 1);
        elfoVerde.adicionarItem(item);
        
        String obtido = elfoVerde.getDescricoesItens();
        assertEquals("", obtido);
    }
    
    @Test public void quandoReceberItemAceitarItemQuePodeUsar() {
        ElfoVerde elfoVerde = new ElfoVerde();
        
        ItemDoInventario item = new ItemDoInventario("Espada de aço valiriano", 1);
        elfoVerde.adicionarItem(item);
        
        String obtido = elfoVerde.getDescricoesItens();
        assertEquals("Espada de aço valiriano", obtido);
    }
    
    @Test public void quandoReceberVariosItensAceitarItensQuePodeUsar() {
        ElfoVerde elfoVerde = new ElfoVerde();
        
        ItemDoInventario item1 = new ItemDoInventario("Espada de aço valiriano", 1);
        ItemDoInventario item2 = new ItemDoInventario("Espada Bastarda", 1);
        ItemDoInventario item3 = new ItemDoInventario("Machado Gigante", 1);
        ItemDoInventario item4 = new ItemDoInventario("Arco e Flecha de vidro", 1);
        ItemDoInventario item5 = new ItemDoInventario("Adaga do Chaos", 1);
        
        elfoVerde.adicionarItem(item1);
        elfoVerde.adicionarItem(item2);
        elfoVerde.adicionarItem(item3);
        elfoVerde.adicionarItem(item4);
        elfoVerde.adicionarItem(item5);
        
        String obtido = elfoVerde.getDescricoesItens();
        assertEquals("Espada de aço valiriano, Arco e Flecha de vidro", obtido);
    }
}
