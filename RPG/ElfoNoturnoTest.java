

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Cenarios de teste para o objeto ElfoNoturno.
 *
 * @author  CWI Software
 * @version 16-09-2014 14:47
 */
public class ElfoNoturnoTest
{
    private double TOLERANCIA = 0.9;
    
    @Test public void quandoAtirarFlechaGanharTresDeExperiencia() {
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        elfoNoturno.atirarFlecha(new Orc());
        
        int obtido = elfoNoturno.getExperiencia();
        assertEquals(3, obtido);
    }
    
    @Test public void quandoAtirarTresFlechasGanharNoveDeExperiencia() {
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        
        elfoNoturno.atirarFlecha(new Orc());
        elfoNoturno.atirarFlecha(new Orc());
        elfoNoturno.atirarFlecha(new Orc());
        
        int obtido = elfoNoturno.getExperiencia();
        assertEquals(9, obtido);
    }
    
    @Test public void quandoAtirarFlechaPerderCincoPorcentoDaVida() {
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        elfoNoturno.atirarFlecha(new Orc());
        double obtido = elfoNoturno.getVida();
        assertEquals(95.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoAtirarTresFlechasPerderVida() {
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        
        elfoNoturno.atirarFlecha(new Orc());
        elfoNoturno.atirarFlecha(new Orc());
        elfoNoturno.atirarFlecha(new Orc());
        
        double obtido = elfoNoturno.getVida();
        assertEquals(85.7, obtido, TOLERANCIA);
    }
    
    @Test public void quandoElfoAtirarFlechasAteMorrerStatusMorto() {
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        
        while((int)elfoNoturno.getVida() > 0) {
            elfoNoturno.alteraQuantidadeFlechas(1);
            elfoNoturno.atirarFlecha(new Orc());
        }
        
        /*for(int i = 1; i != 200000; i++){
            elfoNoturno.alteraQuantidadeFlechas(1);
            elfoNoturno.atirarFlecha(new Orc());            
        }*/
        
        Status obtido = elfoNoturno.getStatus();
        assertEquals(Status.MORTO, obtido);
    }
    
    @Test public void quandoElfoAtirarFlechasAteMorrerVidaFicarEmZero() {
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        
        while((int)elfoNoturno.getVida() > 0) {
            elfoNoturno.alteraQuantidadeFlechas(1);
            elfoNoturno.atirarFlecha(new Orc());
        }
        
        double obtido = elfoNoturno.getVida();
        assertEquals(0.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoElfoMortoNaoAtirarMaisFlechas() {
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        
        int esperado = elfoNoturno.getFlechas();
        
        elfoNoturno.setStatus(Status.MORTO);
        elfoNoturno.atirarFlecha(new Orc());
        elfoNoturno.atirarFlecha(new Orc());
        elfoNoturno.atirarFlecha(new Orc());
        
        int obtido = elfoNoturno.getFlechas();
        
        assertEquals(esperado, obtido);
    }
}
