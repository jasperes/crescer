

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Cenários de teste para o objeto Personagem.
 *
 * @author  CWI Software.
 * @version 15-09-2014 16:37
 */
public class PersonagemTest
{
    private double TOLERANCIA = 0.9;
    
    /**
     * Method chamado antes de cada Method de Teste.
     */
    @Before public void setUp() {
        // Zera contadorDePersonagem.
        Personagem.resetContadorPersonagem();
    }
    
    // Verificação de criação do personagem
    @Test public void quandoCriarPersonagemNascerCom100Vida() {
        Personagem personagem = new Elfo();
        
        double obtido = personagem.getVida();
        assertEquals(100.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoCriarPersonagemNascerCom100Magia() {
        Personagem personagem = new Elfo();
        
        double obtido = personagem.getMagia();
        assertEquals(100.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoCriarPersonagemNascerVivo() {
        Personagem personagem = new Elfo();
        
        Status obtido = personagem.getStatus();
        assertEquals(Status.VIVO, obtido);
    }
    
    @Test public void quandoMorrerAlterarStatusParaMorto() {
        Personagem personagem = new Elfo();
        
        personagem.alteraVida(-1 * (personagem.getVida()+20));
        Status obtido = personagem.getStatus();
        assertEquals(Status.MORTO, obtido);
    }
    
    // Itens do inventário
    @Test public void quandoCriarPersonagemNascerSemItem() {
        Personagem personagem = new Elfo();
        
        String obtido = personagem.getDescricoesItens();
        assertEquals("", obtido);
    }
    
    @Test public void quandoPersonagemGanharItemCriarApenasUmItemESemVirgula() {
        Personagem personagem = new Elfo();
        String descricao = "Machado Gigante";
        ItemDoInventario item = new ItemDoInventario(descricao, 1);
        
        personagem.adicionarItem(item);
        
        String obtido = personagem.getDescricoesItens();
        assertEquals(descricao, obtido);
    }
    
    @Test public void quandoPersonagemGanharDoisItensCriarVirgula() {
        Personagem personagem = new Elfo();
        
        String[] itens = new String[2];
        itens[0] = "Espada";
        itens[1] = "Escudo";
        
        ItemDoInventario item1 = new ItemDoInventario(itens[0], 1);
        ItemDoInventario item2 = new ItemDoInventario(itens[1], 1);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        
        StringBuilder esperado = new StringBuilder();
        esperado.append(itens[0]);
        esperado.append(", ");
        esperado.append(itens[1]);
        
        String obtido = personagem.getDescricoesItens();
        assertEquals(esperado.toString(), obtido);
    }
    
    @Test public void quandoPersonagemGanharCincoItensExibirApenasCincoNoInventario() {
        Personagem personagem = new Elfo();
        
        String[] itens = new String[5];
        itens[0] = "A";
        itens[1] = "B";
        itens[2] = "C";
        itens[3] = "D";
        itens[4] = "E";
        
        ItemDoInventario item1 = new ItemDoInventario(itens[0], 1);
        ItemDoInventario item2 = new ItemDoInventario(itens[1], 1);
        ItemDoInventario item3 = new ItemDoInventario(itens[2], 1);
        ItemDoInventario item4 = new ItemDoInventario(itens[3], 1);
        ItemDoInventario item5 = new ItemDoInventario(itens[4], 1);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        personagem.adicionarItem(item3);
        personagem.adicionarItem(item4);
        personagem.adicionarItem(item5);
        
        StringBuilder esperado = new StringBuilder();
        
        for(String item : itens) {
            if(esperado.toString().isEmpty() == false) {
                esperado.append(", ");
            }
            esperado.append(item);
        }
        
        String obtido = personagem.getDescricoesItens();
        assertEquals(esperado.toString(), obtido);
    }
    
    @Test public void quandoPersonagemGanharEPerdeItemVerificarSeItemFoiRemovidoDaLista() {
        Personagem personagem = new Elfo();
        ItemDoInventario item = new ItemDoInventario("Corda", 1);
        
        personagem.adicionarItem(item);
        
        boolean obtido = personagem.perderItem(item);
        assertEquals(true, obtido);
    }
    
    @Test public void quandoPersonagemGanharEPerderItemFicarSemItensNoInventario() {
        Personagem personagem = new Elfo();
        ItemDoInventario item = new ItemDoInventario("Corda", 1);
        
        personagem.adicionarItem(item);
        personagem.perderItem(item);
        
        String obtido = personagem.getDescricoesItens();
        assertEquals("", obtido);
    }
    
    @Test public void quandoPersonagemGanharEPerderItensVerificarSeItemFoiRemovidoDaLista() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("Pedra", 1);
        ItemDoInventario item2 = new ItemDoInventario("Papel", 1);
        ItemDoInventario item3 = new ItemDoInventario("Tesoura", 1);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        personagem.adicionarItem(item3);
        
        personagem.perderItem(item1);
        personagem.perderItem(item2);
        personagem.perderItem(item3);
        
        String obtido = personagem.getDescricoesItens();
        assertEquals("", obtido);
    }
    
    @Test public void quandoPersonagemGanhaDuasBalasEComeUmaFicaSoComUmaBala() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("Bala Azul", 1);
        ItemDoInventario item2 = new ItemDoInventario("Bala Vermelha", 1);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        
        personagem.perderItem(item2);
        
        String obtido = personagem.getDescricoesItens();
        assertEquals("Bala Azul", obtido);
    }
    
    // getItemComMaiorQuantidade()
    @Test public void verificaItemComMaisQuantidadeEntreDoisItens() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("A", 5);
        ItemDoInventario item2 = new ItemDoInventario("B", 1);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        
        ItemDoInventario obtido = personagem.getItemComMaiorQuantidade();
        
        assertEquals(item1, obtido);
    }
    
    @Test public void verificaItemComMaisQuantidadeEntreDoisItensInvertido() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("A", 1);
        ItemDoInventario item2 = new ItemDoInventario("B", 3);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        
        ItemDoInventario obtido = personagem.getItemComMaiorQuantidade();
        
        assertEquals(item2, obtido);
    }
    
    @Test public void verificaItemComMaisQuantidadeEntreSeixItens() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("A", 1);
        ItemDoInventario item2 = new ItemDoInventario("B", 3);
        ItemDoInventario item3 = new ItemDoInventario("C", 9);
        ItemDoInventario item4 = new ItemDoInventario("D", 12);
        ItemDoInventario item5 = new ItemDoInventario("E", 5);
        ItemDoInventario item6 = new ItemDoInventario("F", 11);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        personagem.adicionarItem(item3);
        personagem.adicionarItem(item4);
        personagem.adicionarItem(item5);
        personagem.adicionarItem(item6);
        
        ItemDoInventario obtido = personagem.getItemComMaiorQuantidade();
        
        assertEquals(item4, obtido);
    }
    
    // ordenaItens()
    @Test public void quandoPersonagemTemItensPosicionadosInvertido() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("B", 5);
        ItemDoInventario item2 = new ItemDoInventario("A", 1);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        
        personagem.ordenaItens();
        
        String obtido = personagem.getDescricoesItens();
        assertEquals("A, B", obtido);
    }
    
    @Test public void quandoPersonagemTemTresItensNaoOrdenadosEDeTamanhosDiferentes() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("C", 5);
        ItemDoInventario item2 = new ItemDoInventario("A", 1);
        ItemDoInventario item3 = new ItemDoInventario("B", 3);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        personagem.adicionarItem(item3);
        
        personagem.ordenaItens();
        
        String obtido = personagem.getDescricoesItens();
        assertEquals("A, B, C", obtido);
    }
    
    @Test public void quandoPersonagemTemTresItensDeMesmoTamanho() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("A", 1);
        ItemDoInventario item2 = new ItemDoInventario("B", 1);
        ItemDoInventario item3 = new ItemDoInventario("C", 1);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        personagem.adicionarItem(item3);
        
        personagem.ordenaItens();
        
        String obtido = personagem.getDescricoesItens();
        assertEquals("A, B, C", obtido);
    }
    
    @Test public void quandoPersonagemTemTresItensNaoOrdenadosEDoisDeMesmoTamanho() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("1", 1);
        ItemDoInventario item2 = new ItemDoInventario("1", 1);
        ItemDoInventario item3 = new ItemDoInventario("3", 3);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        personagem.adicionarItem(item3);
        
        personagem.ordenaItens();
        
        String obtido = personagem.getDescricoesItens();
        assertEquals("1, 1, 3", obtido);
    }
    
    @Test public void quandoPersonagemTemVariosItensNaoOrdenadosEAlgunsDeMesmaQuantidade() {
        Personagem personagem = new Elfo();
        ItemDoInventario item1 = new ItemDoInventario("1", 1);
        ItemDoInventario item2 = new ItemDoInventario("2", 2);
        ItemDoInventario item3 = new ItemDoInventario("3", 3);
        ItemDoInventario item4 = new ItemDoInventario("5", 5);
        ItemDoInventario item5 = new ItemDoInventario("9", 9);
        ItemDoInventario item6 = new ItemDoInventario("16", 16);
        ItemDoInventario item7 = new ItemDoInventario("13", 13);
        ItemDoInventario item8 = new ItemDoInventario("14", 14);
        ItemDoInventario item9 = new ItemDoInventario("8", 8);
        ItemDoInventario item10 = new ItemDoInventario("4", 4);
        ItemDoInventario item11 = new ItemDoInventario("6", 6);
        ItemDoInventario item12 = new ItemDoInventario("17", 17);
        ItemDoInventario item13 = new ItemDoInventario("15", 15);
        ItemDoInventario item14 = new ItemDoInventario("10", 10);
        ItemDoInventario item15 = new ItemDoInventario("7", 7);
        ItemDoInventario item16 = new ItemDoInventario("12", 12);
        ItemDoInventario item17 = new ItemDoInventario("11", 11);
        
        personagem.adicionarItem(item1);
        personagem.adicionarItem(item2);
        personagem.adicionarItem(item3);
        personagem.adicionarItem(item4);
        personagem.adicionarItem(item5);
        personagem.adicionarItem(item6);
        personagem.adicionarItem(item7);
        personagem.adicionarItem(item8);
        personagem.adicionarItem(item9);
        personagem.adicionarItem(item10);
        personagem.adicionarItem(item11);
        personagem.adicionarItem(item12);
        personagem.adicionarItem(item13);
        personagem.adicionarItem(item14);
        personagem.adicionarItem(item15);
        personagem.adicionarItem(item16);
        personagem.adicionarItem(item17);
        
        personagem.ordenaItens();
        
        String esperado = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17";
        
        String obtido = personagem.getDescricoesItens();
        assertEquals(esperado, obtido);
    }
    
    // contador
    @Test public void quandoCriarPersonagemAumentarContador() {
        Personagem personagem = new Elfo();
        
        assertEquals(1, Personagem.getContadorPersonagem());
    }
    
    @Test public void quandoCriarVariosPersonagensAumentarContador() {
        Personagem elfo = new Elfo();
        Personagem elfoVerde = new ElfoVerde();
        Personagem elfoNoturno = new ElfoNoturno();
        Personagem orc = new Orc();
        
        assertEquals(4, Personagem.getContadorPersonagem());
    }
}
