
/**
 * Classe que define um elfo no sistema.
 * 
 * @author CWI Software
 * @version 11-09-2014 09:05
 */
public class Elfo extends Personagem
{
    protected int idade, flechas;
    private static int contadorDeElfos;

    /**
     * Construtor do objeto Elfo com campos padrões.
     */
    public Elfo() {
        super();
        Elfo.contadorDeElfos++;
        this.flechas = 42;
    }
    
    /**
     * Construtor do objeto Elfo com nome;
     * 
     * @param Nome Nome do Elfo.
     */
    public Elfo(String nome) {
        this();
        this.nome = nome;
    }
    
    /**
     * Construtor do objeto Elfo com nome e número de flechas;
     * 
     * @param Nome Nome do Elfo.
     * @param Flechas Quantidade de flechas do Elfo.
     */
    public Elfo(String nome, int flechas) {
        this();
        this.nome = nome;
        this.flechas = flechas;
    }
    
    // Methods Get
    public int getFlechas() {
        return this.flechas;
    }
    
    public int getIdade() {
        return this.idade;
    }
    
    public static int getContadorDeElfos() {
        return Elfo.contadorDeElfos;
    }
    
    public static void resetContadorElfo() {
        Elfo.contadorDeElfos = 0;
    }
    
    // Methods
    protected boolean alteraQuantidadeFlechas(int quantidade) {
        if(this.flechas + quantidade >= 0) {
            this.flechas += quantidade;
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Atira flecha em Orc, aumentando experiencia e diminuindo quantidade de flechas.
     * 
     * @param Orc Objeto Orc à ser atingido pela flecha.
     */
    public void atirarFlechaCerteira(Orc orc) {
        if(this.status == Status.MORTO) {
            return;
        }
        
        if(this.alteraQuantidadeFlechas(-1)) {
            this.experiencia++;
            orc.alteraVida(-10);
        }
    }
    
    /**
     * Atira flecha em Orc, aumentando experiencia e diminuindo quantidade de flechas.
     * 
     * @param Orc Objeto Orc à ser atingido pela flecha.
     */
    public void atirarFlecha(Orc orc) {
        if(this.status == Status.MORTO) {
            return;
        }
        
        if(this.alteraQuantidadeFlechas(-1)) {
            this.experiencia++;
            orc.recebeFlecha(-10);
        }
    }
    
    /**
     * Imprime uma frase informando o nome, quantos flechas e qual o nível de experiencia
     * 
     * @return Frase sobre o Elfo, quantas flechas e qual nível de experiencia.
     * "Legolas possui 42 flechas e 4 niveis de experiencia"
     */
    public String toString(){
             StringBuilder builder = new StringBuilder();
           
             builder.append ( String.format("%s possui %d %s e %d %s de experiência.",
             this.nome,
             this.flechas,
             this.flechas != 1 ? "flechas" : "flecha",
             this.experiencia,
             this.experiencia != 1 ? "níveis" : "nível"));
             
             return builder.toString();
    }
    
    public void atacarPersonagem(Personagem personagem) {}
}
