

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Cenários para teste do objeto Orc.
 *
 * @author  CWI Softwares
 * @version 11-09-2014 11:17
 */
public class OrcTest
{
    private double TOLERANCIA = 0.0000009;
    
    /**
     * Method chamado antes de cada Method de Teste.
     */
    @Before public void setUp() {
        // Zera contadorDeOrc.
        Orc.resetContadorOrc();
    }
    
    @Test public void quandoCriarOrcPadraoNascerCom110Vida() {
        Orc orc = new Orc();
        double obtido = orc.getVida();
        assertEquals(110.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoLevarDanoDiminuirVida() {
        Orc orc  = new Orc("", 110.0);
        orc.alteraVida(-10);
        double obtido = orc.getVida();
        assertEquals(100.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoLevar55DeDanoDiminuirVida() {
        Orc orc = new Orc("", 110.0);
        orc.alteraVida(-55);
        double obtido = orc.getVida();
        assertEquals(55.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoLevar10DanoERecuperar10ContinuarCom110Vida() {
        Orc orc = new Orc("", 110.0);
        orc.alteraVida(-10);
        orc.alteraVida(10);
        double obtido = orc.getVida();
        assertEquals(110.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoLevarFlechadaPerder10Vida() {
        Orc orc = new Orc("", 110.0);
        Elfo elfo = new Elfo();
        elfo.atirarFlechaCerteira(orc);
        double obtido = orc.getVida();
        assertEquals(100.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoLevarDuasFlechadasPerder20Vida() {
        Orc orc = new Orc("", 110.0);
        Elfo elfo = new Elfo();
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        double obtido = orc.getVida();
        assertEquals(90.0, obtido, TOLERANCIA);
        
    }
    
    @Test public void quandoLevarSeteFlechadasPerder70Vida() {
        Orc orc = new Orc("", 110.0);
        Elfo elfo = new Elfo();
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        double obtido = orc.getVida();
        assertEquals(40.0, obtido, TOLERANCIA);
        
    }
    
    @Test public void quandoOrcPerderTodaVidaStatusAlteradoParaMorto() {
        Orc orc = new Orc("", 110.0);
        orc.alteraVida(-1 * orc.getVida());
        Status obtido = orc.getStatus();
        assertEquals(Status.MORTO, obtido);
    }
    
    @Test public void quandoOrcPerderMetadeDaVidaStatusContinuarVIVO() {
        Orc orc = new Orc("", 110.0);
        orc.alteraVida(-55.0);
        Status obtido = orc.getStatus();
        assertEquals(Status.VIVO, obtido);
    }
    
    @Test public void quandoLevarDanoAteDepoisDaMorteVidaZerada() {
        Orc orc = new Orc("", 110.0);
        orc.alteraVida(-1 * (orc.getVida()+10));
        double obtido = orc.getVida();
        assertEquals(0.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoOrcReceberUmaFlechadaStatusVivo() {
        Orc orc = new Orc("", 110.0);
        Elfo elfo = new Elfo();
        elfo.atirarFlechaCerteira(orc);
        Status obtido = orc.getStatus();
        assertEquals(Status.VIVO, obtido);
    }
    
    @Test public void quandoOrcReceberFlechadaAteMorrerStatusMorto() {
        Orc orc = new Orc("", 110.0);
        Elfo elfo = new Elfo();
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        Status obtido = orc.getStatus();
        assertEquals(Status.MORTO, obtido);
    }
    
        @Test public void quandoOrcReceberFlechadaAteDepoisMorrerStatusMorto() {
        Orc orc = new Orc("", 110.0);
        Elfo elfo = new Elfo();
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        Status obtido = orc.getStatus();
        assertEquals(Status.MORTO, obtido);
    }
    
    @Test public void quandoLevarFlechadasAteQuaseMorrerStatusAindaVivo() {
        Orc orc = new Orc("", 110.0);
        Elfo elfo = new Elfo();
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        Status obtido = orc.getStatus();
        assertEquals(Status.VIVO, obtido);
    }
    
    @Test public void quandoLevarFlechadasAteDepoisMorreVidaZerada() {
        Orc orc = new Orc("", 110.0);
        Elfo elfo = new Elfo();
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        elfo.atirarFlechaCerteira(orc);
        double obtido = orc.getVida();
        assertEquals(0.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoLevarDanoAteMorrerVidaZerada() {
        Orc orc = new Orc("", 110.0);
        orc.alteraVida(-1 * orc.getVida());
        double obtido = orc.getVida();
        assertEquals(0.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoCriarOrcNasceCom110() {
        // Arrange - Montar dados de teste
        // Act - Ação de teste (new Orc())
        Orc orc = new Orc();
        // Assert - verificar se tudo deu certo.
        assertEquals(110.0, orc.getVida(), TOLERANCIA);
    }

    @Test public void quandoOrcRecebeUmaFlecha() {
        // AAA
        // Arrange = montagem dos dados de teste
        Orc orc = new Orc();
        double esperado = 100.0;
        // Act = invocar a ação do teste
        orc.recebeFlecha(-10);
        // Assert = verificar se deu tudo certo
        double obtido = orc.getVida();

        assertEquals(esperado, obtido, TOLERANCIA);
    }

    @Test public void quandoOrcRecebeDuasFlechas() {
        // AAA
        // Arrange = montagem dos dados de teste
        Orc orc = new Orc();
        double esperado = 90.0;
        // Act = invocar a ação do teste
        orc.recebeFlecha(-10);
        orc.recebeFlecha(-10);
        // Assert = verificar se deu tudo certo
        double obtido = orc.getVida();

        assertEquals(esperado, obtido, TOLERANCIA);
    }

    @Test public void quandoOrcRecebeTresFlechas() {
        // AAA
        // Arrange = montagem dos dados de teste
        Orc orc = new Orc();
        double esperado = 80.0;
        // Act = invocar a ação do teste
        orc.recebeFlecha(-10);
        orc.recebeFlecha(-10);
        orc.recebeFlecha(-10);
        // Assert = verificar se deu tudo certo
        double obtido = orc.getVida();

        assertEquals(esperado, obtido, TOLERANCIA);
    }
    
    @Test public void quandoChamoToString() {
        Orc orc = new Orc();
        String esperado = "Vida atual: 110.0";
        String obtido = orc.toString();
        assertEquals(esperado, obtido);
    }
    
    @Test public void quandoChamoToStringAposUmaFlecha() {
        Orc orc = new Orc();
        String esperado = "Vida atual: 100.0";
        orc.recebeFlecha(-10);
        String obtido = orc.toString();
        assertEquals(esperado, obtido);
    }

    @Test public void quandoChamoToStringAposDuasFlechas() {
        Orc orc = new Orc();
        String esperado = "Vida atual: 90.0";
        orc.recebeFlecha(-10);
        orc.recebeFlecha(-10);
        String obtido = orc.toString();
        assertEquals(esperado, obtido);
    }
    
    @Test public void quandoOrcPossuiDadosParaLevarFlechadaPerderVida() {
        Orc orc = new Orc("Orcinho", 50.0);
        orc.setStatus(Status.FUGINDO);
        
        Elfo elfo = new Elfo();
        elfo.atirarFlecha(orc);
        
        // lógica para chegar no valor esperado
        // dentro do algoritimo feito no method gerarNumero()
        // int esperado = ((65.0 * 2)/2);
        // esperado *= esperado;
        
        double obtido = orc.getVida();
        
        assertEquals(40.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoPossuirDadosParaNaoLevarFlechadaEGanharExperiencia() {
        Orc orc = new Orc("Orc", 15);
        orc.setStatus(Status.CAÇANDO);
        orc.setExperiencia(-1);
        
        Elfo elfo = new Elfo();
        elfo.atirarFlecha(orc);
        
        // lógica para chegar no valor esperado
        // dentro do algoritimo feito no method gerarNumero()
        // int esperado = ((-60.0 * 3)+1);
        // esperado = (esperado * experado) * esperado;
        
        int obtido = orc.getExperiencia();
        assertEquals(1, obtido);
    }
    
    @Test public void quandoPossuirDadosParaNaoGanharExperiencia() {
        Orc orc = new Orc("", 15.0);
        orc.setStatus(Status.DORMINDO);
        
        Elfo elfo = new Elfo();
        elfo.atirarFlecha(orc);
        
        // lógica para chegar no valor esperado
        // dentro do algoritimo feito no method gerarNumero()
        // int esperado = ((0.0 * 3)+1);
        // esperado *= esperado;
        
        int obtido = orc.getExperiencia();
        assertEquals(0, obtido);
    }
    
    @Test public void quandoPossuirDadosParaNaoLevarFlechada() {
        Orc orc = new Orc("", 15.0);
        orc.setStatus(Status.DORMINDO);
        
        Elfo elfo = new Elfo();
        elfo.atirarFlecha(orc);
        
        // lógica para chegar no valor esperado
        // dentro do algoritimo feito no method gerarNumero()
        // int esperado = ((0.0 * 3)+1);
        // esperado *= esperado;
        
        double obtido = orc.getVida();
        assertEquals(15.0, obtido, TOLERANCIA);
    }
    
    // contador
    @Test public void quandoCriarOrcAumentarContador() {
        Orc orc = new Orc();
        assertEquals(1, Orc.getContadorDeOrc());
    }
    
    @Test public void quandoCriarVariosOrcAumentarContador() {
        Orc orc1 = new Orc();
        Orc orc2 = new Orc();
        Orc orc3 = new Orc();
        assertEquals(3, Orc.getContadorDeOrc());
    }
}
