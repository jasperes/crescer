import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.HashMap;
import java.util.ArrayList;
/**
 * Suíte de testes de um ExercitoDeElfos.
 *
 * @author CWI Software
 * @version 17-09-2014 16:22
 */
public class ExercitoDeElfosTest
{
    @Test
    public void alistarUmElfo() {
        Elfo elfo = new Elfo();
        HashMap<String, Elfo> exercitoEsperado = new HashMap<>();
        exercitoEsperado.put("Sem Nome", elfo);
        ExercitoDeElfos exercito = new ExercitoDeElfos();

        exercito.alistar(elfo);

        HashMap<String, Elfo> obtido = exercito.getExercito();

        assertEquals(exercitoEsperado, obtido);
    }

    @Test
    public void alistarUmElfoEDoisNoturnos() {
        Elfo elfo = new Elfo();
        ElfoNoturno elfo2 = new ElfoNoturno("Legolas");
        ElfoNoturno elfo3 = new ElfoNoturno("Arwen");
        HashMap<String, Elfo> exercitoEsperado = new HashMap<>();
        exercitoEsperado.put("Sem Nome", elfo);
        exercitoEsperado.put("Legolas", elfo2);
        exercitoEsperado.put("Arwen", elfo3);
        ExercitoDeElfos exercito = new ExercitoDeElfos();

        exercito.alistar(elfo);
        exercito.alistar(elfo2);
        exercito.alistar(elfo3);

        HashMap<String, Elfo> obtido = exercito.getExercito();

        assertEquals(exercitoEsperado, obtido);
    }

    @Test
    public void alistarDoisNoturnosEUmVerde() {
        ElfoNoturno elfo2 = new ElfoNoturno("Legolas");
        ElfoVerde coitado = new ElfoVerde("Elfo que queria entrar");
        ElfoNoturno elfo3 = new ElfoNoturno("Arwen");
        HashMap<String, Elfo> exercitoEsperado = new HashMap<>();
        exercitoEsperado.put("Legolas", elfo2);
        exercitoEsperado.put("Arwen", elfo3);
        ExercitoDeElfos exercito = new ExercitoDeElfos();

        exercito.alistar(elfo2);
        exercito.alistar(elfo3);
        exercito.alistar(coitado);

        HashMap<String, Elfo> obtido = exercito.getExercito();

        assertEquals(exercitoEsperado, obtido);
    }

    @Test
    public void buscarPorNull() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo elfoEsperado = null;
        Elfo obtido = exercito.buscar(null);

        assertEquals(elfoEsperado, obtido);
        //assertNull(obtido);
    }

    @Test public void buscarPorElfoSemNome() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo elfo = new Elfo();
        Elfo esperado = elfo;
        exercito.alistar(elfo);

        Elfo obtido = exercito.buscar("Sem Nome");

        assertEquals(esperado, obtido);
    }

    @Test public void buscarPorElfoNoturnoComNomeInformado() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
        Elfo esperado = elfoNoturno;

        exercito.alistar(new Elfo());
        exercito.alistar(elfoNoturno);
        exercito.alistar(new ElfoVerde());
        
        Elfo obtido = exercito.buscar("Legolas");
        assertEquals(esperado, obtido);
    }

    // Ao Alistar
    @Test public void quandoAlistarElfoMortoRecusar() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo elfo = new Elfo();
        elfo.setStatus(Status.MORTO);
        exercito.alistar(elfo);
        
        Elfo obtido = exercito.buscar(elfo.getNome());
        
        assertEquals(null, obtido);
    }
    
    @Test public void quandoAlistarElfoSemVidaRecusar() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo elfo = new Elfo();
        elfo.alteraVida(-1 * elfo.getVida());
        exercito.alistar(elfo);
        
        Elfo obtido = exercito.buscar(elfo.getNome());
        
        assertEquals(null, obtido);
    }
    
    // Grupos por Status
    @Test public void quandoAlistarElfoAgruparEmStatusVivo() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo elfo = new Elfo();
        exercito.alistar(elfo);
        
        ArrayList<Elfo> esperado = new ArrayList<>();
        esperado.add(elfo);
        
        ArrayList<Elfo> obtido = exercito.obterElfosComStatus(Status.VIVO);
        
        assertEquals(esperado, obtido);
    }
    
    @Test public void quandoAlistarTresElfosAgruparEmStatusVivo() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        
        Elfo elfo1 = new Elfo("joao");
        Elfo elfo2 = new Elfo("pedro");
        Elfo elfo3 = new Elfo("maria");
        
        exercito.alistar(elfo1);
        exercito.alistar(elfo2);
        exercito.alistar(elfo3);
        
        ArrayList<Elfo> esperado = new ArrayList<>();
        esperado.add(elfo1);
        esperado.add(elfo2);
        esperado.add(elfo3);
        
        ArrayList<Elfo> obtido = exercito.obterElfosComStatus(Status.VIVO);
        
        assertEquals(esperado, obtido);
    }
    
    @Test public void quandoAlistarElfosComStatusDiferenteAgruparSeparado() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        
        Elfo elfo1 = new Elfo("joao");
        Elfo elfo2 = new Elfo("pedro");
        Elfo elfo3 = new Elfo("maria");
        
        elfo1.setStatus(Status.MORTO);
        elfo2.setStatus(Status.CAÇANDO);
        
        exercito.alistar(elfo1);
        exercito.alistar(elfo2);
        exercito.alistar(elfo3);
        
        ArrayList<Elfo> vivo = new ArrayList<Elfo>();
        vivo.add(elfo2);
        
        ArrayList<Elfo> cacando = new ArrayList<Elfo>();
        cacando.add(elfo3);
        
        HashMap<Status, ArrayList<Elfo>> esperado = new HashMap<>();
        esperado.put(elfo2.getStatus(), vivo);
        esperado.put(elfo3.getStatus(), cacando);
        
        HashMap<Status, ArrayList<Elfo>> obtido = exercito.getGruposPorStatus();
        
        assertEquals(esperado, obtido);
    }
    
    @Test public void quandoAlistarVariosElfosComStatusDiferenteAgruparSeparado() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        
        Elfo elfoA = new Elfo("A");
        Elfo elfoB = new Elfo("B");
        Elfo elfoC = new Elfo("C");
        Elfo elfoD = new Elfo("D");
        Elfo elfoE = new Elfo("E");
        Elfo elfoF = new Elfo("F");
        Elfo elfoG = new Elfo("G");
        Elfo elfoH = new Elfo("H");
        Elfo elfoI = new Elfo("I");
        Elfo elfoJ = new Elfo("J");
        Elfo elfoK = new Elfo("K");
        Elfo elfoL = new Elfo("L");
        Elfo elfoM = new Elfo("M");
        
        elfoA.setStatus(Status.MORTO);
        elfoH.setStatus(Status.MORTO);
        elfoE.setStatus(Status.MORTO);
        
        elfoB.setStatus(Status.VIVO);
        elfoL.setStatus(Status.VIVO);
        
        elfoC.setStatus(Status.CORRENDO);
        elfoF.setStatus(Status.CORRENDO);
        
        elfoG.setStatus(Status.CAÇANDO);
        elfoJ.setStatus(Status.CAÇANDO);
        
        elfoD.setStatus(Status.FUGINDO);
        elfoI.setStatus(Status.FUGINDO);
        
        elfoK.setStatus(Status.DORMINDO);
        elfoM.setStatus(Status.DORMINDO);
        
        exercito.alistar(elfoA);
        exercito.alistar(elfoB);
        exercito.alistar(elfoC);
        exercito.alistar(elfoD);
        exercito.alistar(elfoE);
        exercito.alistar(elfoF);
        exercito.alistar(elfoG);
        exercito.alistar(elfoH);
        exercito.alistar(elfoI);
        exercito.alistar(elfoJ);
        exercito.alistar(elfoK);
        exercito.alistar(elfoL);
        exercito.alistar(elfoM);
        
        ArrayList<Elfo> vivo = new ArrayList<Elfo>();
        vivo.add(elfoB);
        vivo.add(elfoL);
        
        ArrayList<Elfo> correndo = new ArrayList<Elfo>();
        correndo.add(elfoC);
        correndo.add(elfoF);
        
        ArrayList<Elfo> cacando = new ArrayList<Elfo>();
        cacando.add(elfoG);
        cacando.add(elfoJ);
        
        ArrayList<Elfo> fugindo = new ArrayList<Elfo>();
        fugindo.add(elfoD);
        fugindo.add(elfoI);
        
        ArrayList<Elfo> dormindo = new ArrayList<Elfo>();
        dormindo.add(elfoK);
        dormindo.add(elfoM);
        
        ArrayList<Elfo> obtidoMorto = exercito.obterElfosComStatus(Status.MORTO);
        ArrayList<Elfo> obtidoVivo = exercito.obterElfosComStatus(Status.VIVO);
        ArrayList<Elfo> obtidoCorrendo = exercito.obterElfosComStatus(Status.CORRENDO);
        ArrayList<Elfo> obtidoCacando = exercito.obterElfosComStatus(Status.CAÇANDO);
        ArrayList<Elfo> obtidoFugindo = exercito.obterElfosComStatus(Status.FUGINDO);
        ArrayList<Elfo> obtidoDormindo = exercito.obterElfosComStatus(Status.DORMINDO);
        
        assertEquals(null, obtidoMorto);
        assertEquals(vivo, obtidoVivo);
        assertEquals(correndo, obtidoCorrendo);
        assertEquals(cacando, obtidoCacando);
        assertEquals(fugindo, obtidoFugindo);
        assertEquals(dormindo, obtidoDormindo);
    }
}
