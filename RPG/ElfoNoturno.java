
/**
 * Classe que define um Elfo Noturno no sistema.
 * 
 * @author CWI Software
 * @version 16-09-2014 14:40
 */
public class ElfoNoturno extends Elfo
{
    /**
     * Construtor do objeto ElfoNoturno.
     */
    public ElfoNoturno() {
        super();
    }
    
    /**
     * Construtor do objeto ElfoNotunro com nome.
     * 
     * @param Nome Nome do Elfo.
     */
    public ElfoNoturno(String nome) {
        this();
        this.nome = nome;
    }
    
    /**
     * Atira flecha em Orc, aumentando experiencia, diminuindo quantidade de flechas e perdendo 5% da vida atual.
     * 
     * @param Orc Objeto Orc à ser atingido pela flecha.
     */
    public void atirarFlecha(Orc orc) {
        if(this.status == Status.MORTO) {
            return;
        }
        
        super.atirarFlecha(orc);
        this.experiencia += 2;
        
        this.alteraVida(-1 * (this.vida * 0.05));
    }
}
