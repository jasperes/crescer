import java.util.ArrayList;
/**
 * Classe que define um Orc no sistema.
 * 
 * @author CWI Software
 * @version 11-09-2014 09:20
 */
public class Orc extends Personagem
{
    private static int contadorDeOrc;
    
    // Construtores
    /**
     * Construtor do objeto Orc com campos padrões.
     */
    public Orc() {
        super();
        Orc.contadorDeOrc++;
        this.vida = 110.0;
    }
    
    /**
     * Construtor do objeto Orc com parametro Vida.
     * 
     * @param Vida Quantidade da vida do Orc.
     */
    public Orc(String nome, double vida) {
        this();
        this.nome = nome;
        this.vida = vida > 0.0 ? vida : 1;
    }
    
    // Methods Get
    public static int getContadorDeOrc() {
        return Orc.contadorDeOrc;
    }
    
    // Methods Set
    public static void resetContadorOrc() {
        Orc.contadorDeOrc = 0;
    }
    
    // Methods
    /**
     * Algoritimo para verificar se o Orc irá ou não levar flechada no method receberFlecha().
     * O número gerado será feito de acordo com a seguinte lógica:
     * 
     * A. Se o orc possuir nome e o mesmo tiver mais de 5 letras, some 65 ao número.
     * Caso contrário, subtraia 60.
     * 
     * B. Se o orc possuir vida entre 30 e 60, multiple o número por dois, senão se a
     * vida for menor que 20 multiplique por 3.
     * 
     * C. Se o orc estiver fugindo, divida o número por 2. Senão se o orc estiver
     * caçando ou dormindo adicione 1 ao número.
     * 
     * D. Se a experiência do orc for par, eleve o número ao quadrado. Se for ímpar e
     * o orc tiver mais que 2 de experiência, eleve o número ao cubo.
     */
    public double gerarNumero() {
        double numero = 0.0;
        
        // A)
        if(this.nome.length() > 0){
            numero += this.nome.length() > 5 ? 65.0 : -60.0;
        }
        
        // B)
        if(this.vida >= 30.0 && this.vida <= 60.0){
            numero *= 2;
        }else if(this.vida < 20.0){
            numero *= 3;
        }
        
        // C)
        if(this.status == Status.FUGINDO){
            numero /= 2;
        }else if(this.status == Status.CAÇANDO || this.status == Status.DORMINDO){
            numero += 1;
        }
        
        // D)
        if(experiencia % 2 == 0.0){
            numero *= numero;
        }else{
            numero = (numero * numero) * numero;
        }
        
        return numero;
    }
    
    /**
     * Ação realizada quando Orc for receber uma flechada.
     * Para verificar a ação, é gerado um número pelo método gerarNumero().
     * 
     * - Caso o número gerado seja menor que zero, Orc não recebe flechada e aumenta experiencia em 2.
     * - Caso o número gerado seja entre 0 e 100, nada acontece.
     * - Caso o número gerado seja acima de 100, orc recebe flechada.
     * 
     * Se vida zerada, altera Status do Orc para MORTO.
     * Se vida acima de zero, altera Status do Orc para VIVO.
     * 
     * @param Quantidade Quantidade à ser alterada na vida do Orc caso leve a flechada.
     */
    public void recebeFlecha(double quantidade){
        double numero = this.gerarNumero();
        
        if(numero < 0.0){
            this.experiencia += 2;
        }else if(numero >= 0.0 && numero <= 100.0){
            return;
        }else{
            this.alteraVida(quantidade);
        }
    }
    
    /**
     * Imprime vida atual do orc.
     * 
     * @return Vida atual do Orc no seguinte formato de exemplo
     * "Vida atual: 110"
     */
    public String toString(){
        // O código abaixo não funciona.
        // Exception: java.util.IllegalFormatConversionException(null)
        /*StringBuilder builder = new StringBuilder();
        builder.append(String.format("Vida atual: %d", this.vida));
        return builder.toString();*/
        
        return "Vida atual: " + this.vida;
    }
    
    public void atacarPersonagem(Personagem personagem) {
        
    }
}
