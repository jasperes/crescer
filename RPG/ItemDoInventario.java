
/**
 * Write a description of class ItensDoInventario here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ItemDoInventario
{
    private String descricao;
    private int quantidade;

    /**
     * Constructor for objects of class ItensDoInventario
     */
    public ItemDoInventario(String descricao, int quantidade)
    {
        this.descricao = descricao;
        this.quantidade = quantidade;

    }

    public String getDescricao() {
        return this.descricao;
    }
    
    public int getQuantidade() {
        return this.quantidade;
    }

}
