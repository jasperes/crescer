

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Cenários para teste do objeto Elfo.
 *
 * @author  CWI Software
 * @version 11-09-2014 09:36
 */
public class ElfoTest
{
    private double TOLERANCIA = 0.0000009;
    
    /**
     * Method chamado antes de cada Method de Teste.
     */
    @Before public void setUp() {
        // Zera ContadorDeElfos.
        Elfo.resetContadorElfo();
    }
    
    // Ao criar Elfo
    @Test public void quandoCriarElfoPadraoNascerCom100Vida() {
        Elfo elfo = new Elfo();       
        double obtido = elfo.getVida();
        assertEquals(100.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoCriarElfoPadraoNascerSemNome() {
        Elfo elfo = new Elfo();
        String obtido = elfo.getNome();
        assertEquals("Sem Nome", obtido);
    }
    
    @Test public void quandoCriarElfoPadraoNascerCom42Flechas() {
        Elfo elfo = new Elfo();
        int obtido = elfo.getFlechas();
        assertEquals(42, obtido);
    }
    
    @Test public void quandoCriarElfoPadraoNascerSemExperiencia() {
        Elfo elfo = new Elfo();
        int obtido = elfo.getExperiencia();
        assertEquals(0, obtido);
    }
    
    @Test public void quandoCriarElfoPadraoNascerSemIdadeDefinida() {
        Elfo elfo = new Elfo();        
        int obtido = elfo.getIdade();
        assertEquals(0, obtido);
    }
    
    @Test public void quandoElfoPadraoNascerVivo() {
        Elfo elfo = new Elfo();
        
        Status obtido = elfo.getStatus();
        assertEquals(Status.VIVO, obtido);
    }
    
    @Test public void quandoElfoEstiverMortoNaoAtirarMaisFlechas() {
        Elfo elfo = new Elfo("", 5);
        elfo.alteraVida(-1 * elfo.getVida());
        
        int obtido = elfo.getFlechas();
        assertEquals(5, obtido);
    }
    
    @Test public void quandoCriarElfoNascerCom100Vida() {
        Elfo elfo = new Elfo("", 0);
        double obtido = elfo.getVida();
        assertEquals(100.0, obtido, TOLERANCIA);
    }
    
    @Test public void quandoCriarElfoDefinirNome() {
        String nome = "Nome do Elfo";
        Elfo elfo = new Elfo(nome, 0);
        String obtido = elfo.getNome();
        assertEquals(nome, obtido);
    }
    
    @Test public void quandoCriarElfoDefinirFlechas() {
        int flechas = 123;
        Elfo elfo = new Elfo("", flechas);
        int obtido = elfo.getFlechas();
        assertEquals(flechas, obtido);
    }
    
    @Test public void quandoAtirarUmaFlechaDiminuirQuantidadeEm1() {
        Elfo elfo = new Elfo("",30);
        elfo.atirarFlecha(new Orc());
        int obtido = elfo.getFlechas();
        assertEquals(29, obtido);
    }
    
    @Test public void quandoAtirarDuasFlechasDiminuirQuantidadeEm2() {
        Elfo elfo = new Elfo("",30);
        elfo.atirarFlecha(new Orc());
        elfo.atirarFlecha(new Orc());
        int obtido = elfo.getFlechas();
        assertEquals(28, obtido);
    }
    
    @Test public void quandoAtirarSeteFlechasDiminuiQuantidadeEm7() {
        Elfo elfo = new Elfo("", 30);
        elfo.atirarFlecha(new Orc());
        elfo.atirarFlecha(new Orc());
        elfo.atirarFlecha(new Orc());
        elfo.atirarFlecha(new Orc());
        elfo.atirarFlecha(new Orc());
        elfo.atirarFlecha(new Orc());
        elfo.atirarFlecha(new Orc());
        int obtido = elfo.getFlechas();
        assertEquals(23, obtido);
    }
    
    // ContadorDeElfos
    @Test public void quandoCriarElfoAumentarContadorDeElfos() {
        Elfo elfo = new Elfo();
        int obtido = Elfo.getContadorDeElfos();
        assertEquals(1, obtido);
    }
    
    @Test public void quandoCriarElfoNoturnoAumentarContadorDeElfos() {
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        int obtido = Elfo.getContadorDeElfos();
        assertEquals(1, obtido);
    }
    
    @Test public void quandoCriarElfoVerdeAumentarContadorDeElfos() {
        ElfoVerde elfoVerde = new ElfoVerde();
        int obtido = Elfo.getContadorDeElfos();
        assertEquals(1, obtido);
    }
    
    @Test public void quandoCriarTodosElfosAumentarContadorDeElfosEmTresNaClasseElfo() {
        Elfo elfo = new Elfo();
        ElfoVerde elfoVerde = new ElfoVerde();
        ElfoNoturno elfoNoturno = new ElfoNoturno();
        
        int obtido = Elfo.getContadorDeElfos();
        assertEquals(3, obtido);
    }
}
