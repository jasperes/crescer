import java.util.ArrayList;
/**
 * Classe que define um elfo verde no sistema.
 * 
 * @author CWI Software
 * @version 15-09-2014 16:06
 */
public class ElfoVerde extends Elfo
{
    private ArrayList<String> itensQuePodeUsar;
    
    /**
     * Construtor do objeto ElfoVerde.
     */
    public ElfoVerde() {
        super();
        this.adicionarItensQuePodeUsar();
    }
    
    /**
     * Construtor do objeto ElfoVerde com nome.
     * 
     * @param Nome Nome do Elfo.
     */
    public ElfoVerde(String nome) {
        this();
        this.nome = nome;
    }
    
    // Methods
    /**
     * Adiciona os itens à lista de itens que o Elfo Verde pode usar.
     */
    public void adicionarItensQuePodeUsar() {
        this.itensQuePodeUsar = new ArrayList<>();
        this.itensQuePodeUsar.add("espada de aço valiriano");
        this.itensQuePodeUsar.add("arco e Flecha de vidro");
    }
    
    /**
     * Atira flecha em Orc, aumentando experiencia e diminuindo quantidade de flechas.
     * 
     * @param Orc Objeto Orc à ser atingido pela flecha.
     */
    public void atirarFlecha(Orc orc) {
        if(this.status == Status.MORTO) {
            return;
        }
        
        super.atirarFlecha(orc);
        this.experiencia++;
    }
    
    /**
     * Adiciona um item ao inventario.
     * 
     * @param Item Item à ser incluido no inventario.
     */
    public void adicionarItem(ItemDoInventario item) {
        for(String itemOk : itensQuePodeUsar) {
            if(item.getDescricao().toLowerCase().equals(itemOk.toLowerCase())) {
                super.adicionarItem(item);
                break;
            }
        }
    }
}
