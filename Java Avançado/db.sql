CREATE TABLE MERCADORIA 
                            ( IDMERCADORIA INTEGER(10) NOT NULL,
                              DESCRICAO         VARCHAR2(60) NOT NULL,
                              CODIGO               VARCHAR2(36) NOT NULL,
                             PRIMARY KEY (IDMERCADORIA)
                            );

CREATE TABLE HISTORICOENTREGA
                            ( IDHISTORICOENTREGA INTEGER(10) NOT NULL,
                              IDMERCADORIA INTEGER(10) NOT NULL,
                              DATA         VARCHAR2(12) NOT NULL,
                              LOCAL      VARCHAR2(60) NOT NULL,
                              SITUACAO VARCHAR(5) NOT NULL,
                             PRIMARY KEY (IDHISTORICOENTREGA),
              CONSTRAINT HISTORICOENTREGA_FK
              FOREIGN KEY (IDMERCADORIA)
               REFERENCES MERCADORIA (IDMERCADORIA )
                            );