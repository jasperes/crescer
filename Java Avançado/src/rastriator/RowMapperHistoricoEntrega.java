package rastriator;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RowMapperHistoricoEntrega implements RowMapper<HistoricoEntrega>{

	@Override
	public HistoricoEntrega mapRow(ResultSet rs, int i) throws SQLException {
		HistoricoEntrega historico = new HistoricoEntrega();
		historico.setIdHistoricoEntrega(rs.getInt("idHistoricoEntrega"));
		historico.setIdMercadoria(rs.getInt("IdMercadoria"));
		historico.setLocal(rs.getString("local"));
		historico.setData(rs.getString("data"));
		historico.setSituacao(rs.getString("situacao"));
		
		return historico;
	}

}