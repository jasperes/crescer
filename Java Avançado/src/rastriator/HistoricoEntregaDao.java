package rastriator;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

public class HistoricoEntregaDao {
	private JdbcTemplate jdbc;
	
	/**
	 * Construtor do objeto de conexão com banco de dados HistoricoMercadoriaDao.
	 * Através deste objeto é realizado as operações do banco de dados,
	 * tais como inserção, alteração ou remoção da tabela HistoricoEntrega no DB.
	 */
	public HistoricoEntregaDao() {
		// Cria objeto JDBC que se conecta com o banco de dados
		// Através das configurações da classe DataSourceFactory.
		jdbc = new JdbcTemplate(DataSourceFactory.getDataSource());
	}
	
	/**
	 * Gera uma nova ID para o objeto HistoricoMercadoria.
	 * @return Nova id gerada.
	 */
	public int genNewId() {
		String sql = "select HISTORICOMERCADORIA_SEQ.NEXTVAL";
		return jdbc.queryForObject(sql, Integer.class);
	}
	
	/**
	 * Seleciona todo historico de uma mercadoria.
	 * @param idMercadoria ID da mercadoria.
	 * @return Lista de objetos HistoricoEntrega.
	 */
	public List<HistoricoEntrega> selectHistorico(int idMercadoria) {
		String sql = "select * from HistoricoEntrega where idMercadoria = ?";
		return jdbc.query(sql, new RowMapperHistoricoEntrega(), idMercadoria);
	}
	
	/**
	 * Seleciona todo historico de uma mercadoria.
	 * @param codigoMercadoria Codigo da mercadoria.
	 * @return Lista de objetos HistoricoEntrega.
	 */
	public List<HistoricoEntrega> selectHistorico(String codigoMercadoria) {
		String sql = "select * from HistoricoEntrega h "
				+ "inner join Mercadoria m "
				+ "on m.idMercadoria = h.idMercadoria "
				+ "where codigo = ?";
		return jdbc.query(sql, new RowMapperHistoricoEntrega(), codigoMercadoria);
	}
	
	/**
	 * Seleciona todos os históricos das mercadorias.
	 * @return Lista de objetos HistoricoEntrega.
	 */
	public List<HistoricoEntrega> selectAll() {
		String sql = "select * from historicoEntrega";
		return jdbc.query(sql, new RowMapperHistoricoEntrega());
	}
	
	/**
	 * 
	 * @param historico
	 * @return
	 */
	public HistoricoEntrega insert(HistoricoEntrega historico) {
		String sql = "insert into historicoentrega("
				+ "idHistoricoEntrega, idMercadoria, data, local, situacao) "
				+ "values(?, ?, ?, ?, ?)";
		historico.setIdHistoricoEntrega(this.genNewId());
		int idHistoricoEntrega = historico.getIdHistoricoEntrega();
		int idMercadoria = historico.getIdMercadoria();
		String data = historico.getData();
		String local = historico.getLocal();
		String situacao = historico.getSituacao();
		jdbc.update(sql, idHistoricoEntrega, idMercadoria, data, local, situacao);
		return historico;
	}
}
