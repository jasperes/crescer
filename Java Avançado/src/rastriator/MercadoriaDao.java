package rastriator;

import java.util.List;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;

public class MercadoriaDao {
	private JdbcTemplate jdbc;
	
	/**
	 * Construtor do objeto de conexão com banco de dados MercadoriaDao.
	 * Através deste objeto é realizado as operações do banco de dados,
	 * tais como inserção, alteração ou remoção das Mercadorias no DB.
	 */
	public MercadoriaDao() {
		// Cria objeto JDBC que se conecta com o banco de dados
		// Através das configurações da classe DataSourceFactory.
		jdbc = new JdbcTemplate(DataSourceFactory.getDataSource());
	}
	
	/**
	 * Gera uma nova ID para o objeto Mercadoria.
	 * @return Nova id gerada.
	 */
	public int genNewId() {
		String sql = "select MERCADORIA_SEQ.NEXTVAL";
		return jdbc.queryForObject(sql, Integer.class);
	}
	
	/**
	 * Seleciona todas as mercadorias do banco de dados.
	 * @return Lista de objetor Mercadoria.
	 */
	public List<Mercadoria> selectAll() {
		String sql = "select * from mercadoria";
		return jdbc.query(sql, new RowMapperMercadoria());
	}
	
	/**
	 * Seleciona uma mercadoria do banco de dados.
	 * @param idmercadoria ID da mercadoria desejada.
	 * @return Objeto Mercadoria.
	 */
	public Mercadoria selectOne(int idmercadoria) {
		String sql = "select * from mercadoria where idmercadoria = ?";
		return jdbc.query(sql, new RowMapperMercadoria(), idmercadoria).get(0);
	}
	
	/**
	 * Seleciona todas as mercadorias que possuem determinada descricao.
	 * @param descricao Descricao da(s) mercadoria(s) procurada.
	 * @return Lista de objetos Mercadoria.
	 */
	public List<Mercadoria> select(String descricao) {
		String sql = "select * from mercadoria where descricao = ?";
		return jdbc.query(sql, new RowMapperMercadoria(), descricao);
	}
	
	/**
	 * Adiciona uma nova mercadoria gerando uma nova idMercadoria.
	 * @param mercadoria Objeto Mercadoria à inserir no banco de dados.
	 * @return 
	 */
	public Mercadoria insert(Mercadoria mercadoria) {
		String sql = "insert into mercadoria"
				+ "(IdMercadoria, descricao, codigo) "
				+ "values(?, ?, ?)";
		
		mercadoria.setIdMercadoria(this.genNewId());
		mercadoria.setCodigo(UUID.randomUUID().toString());
		
		int idMercadoria = mercadoria.getIdMercadoria();
		String descricao = mercadoria.getDescricao();
		String codigo = mercadoria.getCodigo();
		
		jdbc.update(sql, idMercadoria, descricao, codigo);
		return mercadoria;
	}
	
	/**
	 * Atualiza os dados de uma mercadoria no banco de dados.
	 * @param mercadoria Objeto Mercadoria à atualizar no banco de dados.
	 * @param novaDescricao Nova descrição da mercadoria.
	 */
	public void update(Mercadoria mercadoria, String novaDescricao) {
		String sql = "update mercadoria "
				+ "set descricao = ? "
				+ "where idmercadoria = ?";
		int idMercadoria = mercadoria.getIdMercadoria();
		jdbc.update(sql, novaDescricao, idMercadoria);
	}
	
	/**
	 * Remove uma mercadoria do banco de dados.
	 * @param mercadoria Objeto Mercadoria à remover no banco de dados.
	 */
	public void delete(Mercadoria mercadoria) {
		String sql = "delete from mercadoria "
				+ "where idmercadoria = ?";
		int idMercadoria = mercadoria.getIdMercadoria();
		jdbc.update(sql, idMercadoria);
	}
}
