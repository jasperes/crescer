package rastriator;

public class Mercadoria {
	private int idMercadoria;
	private String descricao;
	private String codigo;
	
	/**
	 * Construtor do objeto Mercadoria.
	 */
	public Mercadoria() {
	}
	
	/**
	 * Construtor do objeto Mercadoria.
	 * 
	 * @param descricao Descrição da mercadoria.
	 */
	public Mercadoria(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Construtor do objeto Mercadoria.
	 * 
	 * @param idMercadoria ID da mercadoria.
	 * @param descricao Descrição da mercadoria.
	 */
	public Mercadoria(int idMercadoria, String descricao, String codigo) {
		this.idMercadoria = idMercadoria;
		this.descricao = descricao;
		this.codigo = codigo;
	}
	
	// Methods Set
	public void setIdMercadoria(int idMercadoria) {
		this.idMercadoria = idMercadoria;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	// Methods Get
	public int getIdMercadoria() {
		return idMercadoria;
	}

	public String getDescricao() {
		return this.descricao;
	}
	
	public String getCodigo() {
		return codigo;
	}
}
