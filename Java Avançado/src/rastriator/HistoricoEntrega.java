package rastriator;

public class HistoricoEntrega {
	private int idHistoricoEntrega;
	private int idMercadoria;
	private String data;
	private String local;
	private String situacao;
	
	/**
	 * Construtor do objeto HistoricoEntrega.
	 */
	public HistoricoEntrega() {}
	
	/**
	 * Construtor do objeto HistoricoEntrega.
	 */
	public HistoricoEntrega(int idMercadoria) {
		
	}
	
	/**
	 * Construtor do objeto HistoricoEntrega.
	 * 
	 * @param idHistoricoEntrega ID do histórico de entrga.
	 * @param idMercadoria ID da mercadoria.
	 * @param data Data do lançamento do histórico.
	 * @param local Local onde a mercadoria está no momento do histórico.
	 * @param situacao Situação da entrega da mercadoria.
	 */
	public HistoricoEntrega(int idHistoricoEntrega, int idMercadoria, String data, String local, String situacao){
		this.idHistoricoEntrega = idHistoricoEntrega;
		this.idMercadoria = idMercadoria;
		this.data = data;
		this.local = local;
		this.situacao = situacao;
	}
	
	// Methods Set
	public String getSituacao() {
		return situacao;
	}
	
	public void setIdHistoricoEntrega(int idHistoricoEntrega) {
		this.idHistoricoEntrega = idHistoricoEntrega;
	}
	
	public void setIdMercadoria(int idMercadoria) {
		this.idMercadoria = idMercadoria;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	public void setLocal(String local) {
		this.local = local;
	}
	
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	// Methods Get
	public int getIdHistoricoEntrega() {
		return idHistoricoEntrega;
	}
	
	public int getIdMercadoria() {
		return idMercadoria;
	}
	
	public String getData() {
		return data;
	}
	
	public String getLocal() {
		return local;
	}

}
