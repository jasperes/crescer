package rastriator;

import static spark.Spark.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import spark.Request;
import spark.Response;

public class Servidor {
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static HistoricoEntregaDao historicoEntregaDao = new HistoricoEntregaDao();
	private static MercadoriaDao mercadoriaDao = new MercadoriaDao();
	
	public static void main(String[] args) {
		
		externalStaticFileLocation(
				System.getProperty("user.dir") + "/resources/static");
		
		before((req, resp) -> resp.header("Access-Control-Allow-Origin", "*"));
		get("/mercadoria/listar", (req, resp) -> listarMercadorias(req, resp));
		post("/mercadoria/salvar", (req, resp) -> savarMercadoria(req, resp));
		post("/mercadoria/historico/detalhes/id", (req, resp) -> detalhesHistoricoMercadoriaId(req, resp));
		post("/mercadoria/historico/detalhes/codigo", (req, resp) -> detalhesHistoricoMercadoriaCodigo(req, resp));
		post("/mercadorias/historico/inserir", (req, resp) -> salvarHistorico(req, resp));
		
	}

	private static String listarMercadorias(Request req, Response resp) {
		return gson.toJson(mercadoriaDao.selectAll());
	}
	
	private static String salvarHistorico(Request req, Response resp) {
		HistoricoEntrega historico = gson.fromJson(req.body(), HistoricoEntrega.class);
		return gson.toJson(historicoEntregaDao.insert(historico));
	}

	private static String savarMercadoria(Request req, Response resp) {
		Mercadoria mercadoria = gson.fromJson(req.body(), Mercadoria.class);
		return gson.toJson(mercadoriaDao.insert(mercadoria));
	}

	private static String detalhesHistoricoMercadoriaId(Request req, Response resp) {
		return gson.toJson(historicoEntregaDao.selectHistorico(Integer.parseInt(req.body())));
	}
	
	private static String detalhesHistoricoMercadoriaCodigo(Request req, Response resp) {
		return gson.toJson(historicoEntregaDao.selectHistorico(req.body()));
	}
}