$(function() {
    function addItemHistorico(data, local, situacao) {
	$("#data").val("");
	$("#local").val("");
	$("#situacao").val("");
	$("#tabela_historico").append(
	    $("<tr>").append(
		$("<td>").text(data)
	    ).append(
		$("<td>").text(local)
	    ).append(
		$("<td>").text(situacao)
	    )
	);
    };
	
    $("#pesquisa_mercadoria").on("click", function() {
        console.log($("#codigo").val());
	$.ajax({
	    type : "POST",
	    url : "/mercadoria/historico/detalhes/codigo",
	    data : JSON.stringify($("#codigo").val()),
	    cache : false,
	    success : function(data) {
            console.log("a");
		$("#tabela_historico").empty();
		$.each($.parseJSON(data), function(i,v){
		    addItemHistorico(v["data"], v["local"], v["situacao"]);
		});

	    }
	});
    });
});
