$(function() {
    function addItemHistorico(data, local, situacao) {
	    $("#data").val("");
	    $("#local").val("");
	    $("#situacao").val("");
	    $("#tabela_historico").append(
	        $("<tr>").append(
	    	    $("<td>").text(data)
	        ).append(
	    	    $("<td>").text(local)
	        ).append(
	    	    $("<td>").text(situacao)
	        )
    	);
    };

    function addItemMenu(id, descricao, codigo) {
	    id = ""+id;
	    $("#menu_mercadorias").append(
	        $("<li>").attr("id","itemMenu_"+id).append(
    		$("<a>").attr("href","#").append(descricao)
        	).on("click", function() {
	        	$("#menu_mercadorias>li").css({"background-color":"white"});
	        	$(this).css({"background-color":"#EEE"});
	        	$("#data").removeAttr("disabled");
    	    	$("#local").removeAttr("disabled");
		        $("#situacao").removeAttr("disabled");
	        })
	    );
	    $("#descricao").val("");
    	$("#menu_mercadorias #itemMenu_"+id).on("click", function() {
	        $.ajax({
	        	type : "POST",
	        	url : "/mercadoria/historico/detalhes/id",
	        	data : id,
                cache : false,
		        success : function(data) {
                    $("#codigo_mercadoria").empty().append(
                        $("<b>").append("Codigo: ")
                    ).append(codigo);
		            $("#tabela_historico").empty().attr("class", id);
		            $.each($.parseJSON(data), function(i,v){
			            addItemHistorico(v["data"], v["local"], v["situacao"]);
		            });
	        	}
	        });
    	});
    };

    $.ajax({
	    type : "GET",
    	url : "/mercadoria/listar",
    	success : function(data) {
	        $.each($.parseJSON(data), function(i,v) {
	    	    addItemMenu(v["idMercadoria"], v["descricao"], v["codigo"]);
	        });
    	}
    });

    $("#cadastrar_mercadoria").on("click", function(){
	    var formData = {"descricao" : $("#descricao").val()};
    	$.ajax({
	        type : "POST",
	        url : "/mercadoria/salvar",
	        data : JSON.stringify(formData),
	        success : function(data) {
		        var nData = $.parseJSON(data);
		        addItemMenu(nData["idMercadoria"], nData["descricao"], nData["codigo"]);
	        }
	    });
    });

    $("#cadastrar_historico").on("click", function() {
	    var formData = {
	        "idMercadoria" : $("#tabela_historico").attr("class"),
	        "data" : $("#data").val(),
	        "local" : $("#local").val(),
	        "situacao" : $("#situacao").val()
	    };
    	$.ajax({
	        type : "POST",
	        url : "/mercadorias/historico/inserir",
	        data : JSON.stringify(formData),
	        success : function(data) {
	    	    var v = $.parseJSON(data)
	    	    addItemHistorico(v["data"], v["local"], v["situacao"]);
	        }
    	});
    });
});
