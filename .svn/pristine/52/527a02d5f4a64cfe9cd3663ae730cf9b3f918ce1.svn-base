package MestreCuca;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class LivroTestes {
	private double tolerancia = 0.0000009;

	// Livro()
	@Test public void quandoCriarLivroListaDeReceitasVazia() {
		LivroReceitas livro = new Livro();
		
		List<Receita> esperado = new ArrayList<>();
		List<Receita> obtido = livro.getTodasReceitas();
		
		assertEquals(esperado, obtido);
	}
	
	// buscaReceitaPeloNome() - testes normais
	/*@Test public void quandoBuscarReceitaRetornarNull() {
		LivroReceitas livro = new Livro();
		
		Receita esperado = null;
		Receita obtido = livro.buscaReceitaPeloNome("nome");
		
		assertEquals(esperado, obtido);
	}*/
	
	@Test public void quandoBuscarReceitaRetornar() {
		LivroReceitas livro = new Livro();
		Receita receita = new Receita("nome");
		livro.inserir(receita);
		
		Receita esperado = receita;
		Receita obtido = livro.buscaReceitaPeloNome("nome");
		
		assertEquals(esperado, obtido);
	}
	
	@Test public void quandoBuscarReceitaQuandoTemVariasReceitasRetornar() {
		LivroReceitas livro = new Livro();
		
		Receita torta = new Receita("torta");
		Receita bolo = new Receita("bolo");
		Receita bomba = new Receita("bomba");
		
		livro.inserir(torta);
		livro.inserir(bolo);
		livro.inserir(bomba);
		
		Receita esperado = bomba;
		Receita obtido = livro.buscaReceitaPeloNome("bomba");
		
		assertEquals(esperado, obtido);
	}
	
	// buscaReceitaPeloNome() - exceptions
	/*@Test(expected = NullPointerException.class)
	public void quandoBuscarNullOcorrerErro() {
		LivroReceitas livro = new Livro();
		
		try{
			livro.buscaReceitaPeloNome(null);
		}finally{
			
		}
	}*/
	
	@Test(expected = ReceitaIncompleta.class)
	public void quandoBuscarENaoEncontrarOcorrerErro() {
		LivroReceitas livro = new Livro();
		
		try{
			livro.buscaReceitaPeloNome("");
		}finally{
			
		}
	}
	
	//getPrecoTotalReceitas()
	@Test public void quandoCriarLivroVerificarPrecoTotalDasReceitas() {
		Livro livro = new Livro();
		
		// Brigadeiro
		Receita brigadeiro = new Receita("Brigadeiro");
		
		Igrediente chocolate = new Igrediente(UnidadeDeMedida.GRAMAS, "Chocolate", 100.0);
		Igrediente leiteCondensado = new Igrediente(UnidadeDeMedida.GRAMAS, "Leite condensado", 150.0);
		Igrediente margarina = new Igrediente(UnidadeDeMedida.GRAMAS, "Margarina", 25.0);
		
		chocolate.setPreco(1.50);
		leiteCondensado.setPreco(2.0);
		margarina.setPreco(2.0);
		
		brigadeiro.adicionaIgrediente(chocolate);
		brigadeiro.adicionaIgrediente(leiteCondensado);
		brigadeiro.adicionaIgrediente(margarina);
		
		// Algo Aleatório 1
		Receita aleatorio1 = new Receita("Brigadeiro");
		
		Igrediente algo1 = new Igrediente(UnidadeDeMedida.GRAMAS, "Chocolate", 100.0);
		
		algo1.setPreco(1.50);
		
		aleatorio1.adicionaIgrediente(algo1);
		
		// Algo Aleatório 2
		Receita aleatorio2 = new Receita("Brigadeiro");
		
		Igrediente algo2 = new Igrediente(UnidadeDeMedida.GRAMAS, "Chocolate", 100.0);
		
		algo2.setPreco(1.50);
		
		aleatorio2.adicionaIgrediente(algo2);
		
		// Adiciona Receitas ao Livro
		livro.inserir(brigadeiro);
		livro.inserir(aleatorio1);
		livro.inserir(aleatorio2);
		
		// variaveis de teste
		double esperado = 8.50;
		double obtido = livro.getPrecoTotalReceitas();
		
		assertEquals(esperado, obtido, tolerancia);
	}
	
	// getReceitasSemDeterminadosIgredientes()
	@Test public void verificarReceitasSemUmIgrediente() {
		Livro livro = new Livro();
		
		Receita torta = new Receita("torta");
		Receita bolo = new Receita("bolo");
		
		Igrediente chocolatePreto = new Igrediente(UnidadeDeMedida.GRAMAS, "chocolate", 150.0);
		Igrediente chocolateBranco = new Igrediente(UnidadeDeMedida.GRAMAS, "chocolate branco", 150.0);
		
		torta.adicionaIgrediente(chocolatePreto);
		bolo.adicionaIgrediente(chocolateBranco);
		
		livro.inserir(torta);
		livro.inserir(bolo);
		
		List<Receita> esperado = new ArrayList<>();
		esperado.add(torta);
		
		List<Igrediente> igredientes = new ArrayList<>();
		igredientes.add(chocolateBranco);
		List<Receita> obtido = livro.getReceitasSemDeterminadosIgredientes(igredientes);
		
		assertEquals(esperado, obtido);
	}
	
	// getQuantidadeTotalIgredientes()
	@Test public void verificaQuantidadeTotalDeIgredientes() {
		Livro livro = new Livro();
		
		Receita torta = new Receita("torta");
		Receita bolo = new Receita("bolo");
		
		Igrediente chocolatePreto = new Igrediente(UnidadeDeMedida.GRAMAS, "chocolate", 150.0);
		Igrediente chocolateBranco = new Igrediente(UnidadeDeMedida.GRAMAS, "chocolate branco", 150.0);
		
		torta.adicionaIgrediente(chocolatePreto);
		torta.adicionaIgrediente(chocolateBranco);
		bolo.adicionaIgrediente(chocolateBranco);
		
		livro.inserir(torta);
		livro.inserir(bolo);
		
		List<String> esperado = new ArrayList<>();
		esperado.add("150.0 Gramas de chocolate.");
		esperado.add("300.0 Gramas de chocolate branco.");
		
		List<String> obtido = livro.getQuantidadeTotalIgredientes();
		
		assertEquals(esperado, obtido);
	}

}
