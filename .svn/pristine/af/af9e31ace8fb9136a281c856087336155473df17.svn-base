import java.util.HashMap;
import java.util.ArrayList;

/**
 * Classe que representa um exército de elfos.
 * 
 * @author CWI Software
 * @version 17-09-2014 16:10
 */
public class ExercitoDeElfos {

    private HashMap<String, Elfo> exercito;
    private HashMap<Status, ArrayList<Elfo>> gruposPorStatus;

    public ExercitoDeElfos() {
        this.exercito = new HashMap<>();
        this.gruposPorStatus = new HashMap<>();
    }

    // Methods Get
    public HashMap<String, Elfo> getExercito() {
        return this.exercito;
    }
    
    public HashMap<Status, ArrayList<Elfo>> getGruposPorStatus() {
        return this.gruposPorStatus;
    }

    // Methods
    /**
     * Adiciona um elfo ao exército, caso ele NÃO seja um ElfoVerde.
     * 
     * @param elfo Elfo a ser adicionado no exército, vinculado a seu nome.
     */
    public void alistar(Elfo elfo) {
        
        // Verifica se o Elfo possui todos os parametros necessários para ser alistado, sendo eles:
        // -Não pode ser ElfoVerde
        // -Não pode ser Null
        // -Deve ter vida maior que zero
        // -Não pode estar Morto
        // -Não pode estar alistado
        boolean podeAlistar = !(elfo instanceof ElfoVerde) && elfo != null && (int)elfo.getVida() > 0 && elfo.getStatus() != Status.MORTO && this.exercito.get(elfo.getNome()) == null;

        // Se estiver ok para ser alistado
        if (podeAlistar) {
            // Alista o elfo no exercito;
            this.exercito.put(elfo.getNome(), elfo);
            
            // Aqui é adicionado o elfo na lista de grupos por status. Para isso:
            // Verifica se o Grupo do Status do Elfo não existe;
            boolean grupoNaoExiste = gruposPorStatus.get(elfo.getStatus()) == null;
            ArrayList<Elfo> grupo;
            
            // Caso não exista:
            if(grupoNaoExiste) {
                // Cria uma nova lista e agrega o elfo à ela;
                grupo = new ArrayList<>();
                grupo.add(elfo);
            // Caso exista:
            }else{
                // Pega a o grupo de elfos do Status e o inclui no final;
                grupo = this.gruposPorStatus.get(elfo.getStatus());
                grupo.add(elfo);
            }
            
            // Por fim, sobreescreve o grupo anterior pelo novo grupo com o Elfo adicionado.
            this.gruposPorStatus.put(elfo.getStatus(), grupo);
        }

    }
    
    /**
     * Agrupa os Elfos alistados no exercito por Status.
     */
    public void agruparElfosPorStatus() {
        // Para cada Elfo alistado no exercito, é adicionado à um grupo pelo seu Status:
        for(Elfo elfo : this.exercito.values()) {
            // Verifica se o Grupo do Status do Elfo não existe;
            boolean grupoNaoExiste = gruposPorStatus.get(elfo.getStatus()) == null;
            ArrayList<Elfo> grupo;
            
            // Caso não exista:
            if(grupoNaoExiste) {
                // Cria uma nova lista e agrega o elfo à ela;
                grupo = new ArrayList<>();
                grupo.add(elfo);
            // Caso exista:
            }else{
                // Pega a o grupo de elfos do Status e o inclui no final;
                grupo = this.gruposPorStatus.get(elfo.getStatus());
                grupo.add(elfo);
            }
            
            // Por fim, sobreescreve o grupo anterior pelo novo grupo com o Elfo adicionado.
            this.gruposPorStatus.put(elfo.getStatus(), grupo);
        }
    }
    
    /**
     * Retorna lista com Elfos de determinado Status.
     * 
     * @param status Status à ser pesquisado.
     * @return ArrayList<Elfo> Lista dos elfos do status pesquisado.
     */
    public ArrayList<Elfo> obterElfosComStatus(Status status) {
        return this.gruposPorStatus.get(status);
    }
    
    /**
     * Busca um Elfo através de seu nome.
     * 
     * @param nome String que representa o nome.
     * @return Objeto Elfo relacionado ao nome.
     */
    public Elfo buscar(String nome) {
        return this.exercito.get(nome);
    }
    
}