
import java.util.ArrayList;

/**
 * Classe que define um Orc no sistema.
 * 
 * @author CWI Software
 * @version 11-09-2014 09:20
 */
public class Orc
{
    private double vida;
    private Status status;
    private int experiencia;
    private String nome;
    private ArrayList<ItemDoInventario> itensDoInventario;
    
    // Construtores
    /**
     * Construtor do objeto Orc com campos padrões.
     */
    public Orc() {
        this.nome = "Sem Nome";
        this.vida = 110.0;
        this.status = Status.VIVO;
        this.itensDoInventario = new ArrayList<>();
    }
    
    /**
     * Construtor do objeto Orc com parametro Vida.
     * 
     * @param Vida Quantidade da vida do Orc.
     */
    public Orc(String nome, double vida) {
        this.nome = nome;
        this.vida = vida;
        this.status = this.vida > 0.0 ? Status.VIVO : Status.MORTO;
        this.itensDoInventario = new ArrayList<>();
    }
    
    // Methods Get
    public double getVida() {
        return this.vida;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    /**
     * @return String Descrições dos itens do Orc.
     */
    public String getDescricoesItens() {
        StringBuilder itens = new StringBuilder();
        
        /*for(int i=0; i < this.itensDoInventario.size(); i++) {
            if(i>0 && i != this.itensDoInventario.size()){itens.append(", ");}
            itens.append((this.itensDoInventario.get(i)).getDescricao());
        }*/
        
        // Para cada item nos itens do inventário:
        // Adiciona o item à string.
        for(ItemDoInventario item : this.itensDoInventario) {
            itens.append(itens.toString().isEmpty() == false ? ", " : "");
            /*if(itens.toString().isEmpty() == false) {
                itens.append(", ");
            }*/
            itens.append(item.getDescricao());
        }
        
        return itens.toString();
    }
    
    public ItemDoInventario getItemComMaiorQuantidade() {
        ItemDoInventario maiorItem = new ItemDoInventario("Nenhum item no inventario", 0);
        
        for(ItemDoInventario item : this.itensDoInventario) {
            if(item.getQuantidade() > maiorItem.getQuantidade()) {
                maiorItem = item;
            }
        }
        
        
        return maiorItem;
    }
    
    // Methods Set
    /*public void vida(double vida) {
        this.vida = vida;
    }*/
    
    public void setStatus(Status status) {
        this.status = status;
    }
    
    public void setExperiencia(int xp) {
        this.experiencia = xp;
    }
    
    // Methods
    /**
     * Adiciona um item ao inventario do Orc.
     * 
     * @param Item Item à ser incluido no inventario do Orc.
     */
    public void adicionarItem(ItemDoInventario item) {
        if(item.getQuantidade() > 0) {
            this.itensDoInventario.add(item);
        }
    }
    
    /**
     * Remove um item do inventario do Orc.
     * 
     * @param Item Item à ser removido do inventario do Orc.
     * @return Boolean Se removido, retorna True. Do contrário, retorna False.
     */
    public boolean perderItem(ItemDoInventario item) {
        if(this.itensDoInventario.contains(item)){
            this.itensDoInventario.remove(this.itensDoInventario.indexOf(item));
            return true;
        }else{
            return false;
        }
    }
    
    
    /**
     * Ordena os itens do inventário por ordem de quantidade.
     */
    public void ordenaItens() {
        ArrayList<ItemDoInventario> itensPosicionados = new ArrayList<>(this.itensDoInventario.size());
        
        
        for(ItemDoInventario c : this.itensDoInventario) {
            itensPosicionados.add(c);
        }
        
        for(ItemDoInventario item : this.itensDoInventario) {
            for(ItemDoInventario itemPosicionado : itensPosicionados){
                
                // Se o item e o item posicionado forem o mesmo objeto, para loop e verifica próximo item.
                if(item == itemPosicionado) {
                    break;
                }
                
                // Se o item for menor do que o item posicionado
                // o item terá que ficar no lugar do item posicionado
                // e o item posicionado na posição abaixo
                if(item.getQuantidade() < itemPosicionado.getQuantidade()) {
                    
                    int indexPosicionado = itensPosicionados.indexOf(itemPosicionado);
                    int indexItem = itensPosicionados.indexOf(item);
                     
                    // move os itens na posição abaixo dele para aqueles entre o item e o item posicionado
                    for(int i = indexItem; i > indexPosicionado; i--) {
                        itensPosicionados.set(i, itensPosicionados.get(i-1));
                    }
                    
                    // coloca o item posicionado na posição abaix e o item no lugar dele
                    itensPosicionados.set(indexPosicionado, item);
                    itensPosicionados.set(indexPosicionado+1, itemPosicionado);
                    
                    break;
                }
            }
        }
        
        this.itensDoInventario = itensPosicionados;
    }
    
    /**
     * Algoritimo para verificar se o Orc irá ou não levar flechada no method receberFlecha().
     * O número gerado será feito de acordo com a seguinte lógica:
     * 
     * A. Se o orc possuir nome e o mesmo tiver mais de 5 letras, some 65 ao número.
     * Caso contrário, subtraia 60.
     * 
     * B. Se o orc possuir vida entre 30 e 60, multiple o número por dois, senão se a
     * vida for menor que 20 multiplique por 3.
     * 
     * C. Se o orc estiver fugindo, divida o número por 2. Senão se o orc estiver
     * caçando ou dormindo adicione 1 ao número.
     * 
     * D. Se a experiência do orc for par, eleve o número ao quadrado. Se for ímpar e
     * o orc tiver mais que 2 de experiência, eleve o número ao cubo.
     */
    private double gerarNumero() {
        double numero = 0.0;
        
        // A)
        if(this.nome.length() > 0){
            numero += this.nome.length() > 5 ? 65.0 : -60.0;
        }
        
        // B)
        if(this.vida >= 30.0 && this.vida <= 60.0){
            numero *= 2;
        }else if(this.vida < 20.0){
            numero *= 3;
        }
        
        // C)
        if(this.status == Status.FUGINDO){
            numero /= 2;
        }else if(this.status == Status.CAÇANDO || this.status == Status.DORMINDO){
            numero += 1;
        }
        
        // D)
        if(experiencia % 2 == 0.0){
            numero *= numero;
        }else{
            numero = (numero * numero) * numero;
        }
        
        return numero;
    }
    
    /**
     * Ação realizada quando Orc for receber uma flechada.
     * Para verificar a ação, é gerado um número pelo método gerarNumero().
     * 
     * - Caso o número gerado seja menor que zero, Orc não recebe flechada e aumenta experiencia em 2.
     * - Caso o número gerado seja entre 0 e 100, nada acontece.
     * - Caso o número gerado seja acima de 100, orc recebe flechada.
     * 
     * Se vida zerada, altera Status do Orc para MORTO.
     * Se vida acima de zero, altera Status do Orc para VIVO.
     * 
     * @param Quantidade Quantidade à ser alterada na vida do Orc caso leve a flechada.
     */
    public void recebeFlecha(double quantidade){
        double numero = this.gerarNumero();
        
        if(numero < 0.0){
            this.experiencia += 2;
        }else if(numero >=0.0 && numero <= 100.0){
            return;
        }else{
            this.vida = this.vida + quantidade < 0.0 ? 0.0 : this.vida + quantidade;
            this.status = this.vida > 0.0 ? this.status : Status.MORTO;
        }
    }
    
    /**
     * Altera vida do objeto Orc.
     * Se vida zerada, altera Status do Orc para MORTO.
     * Se vida acima de zero, altera Status do Orc para VIVO.
     * 
     * @param Quantidade Quantidade à ser alterada na vida do Orc.
     */
    public void alteraVida(double quantidade) {
        // Se a quantidade à somar na vida deixar o Orc com vida abaixo de zero: Vida fica zerada
        // Se não: Soma quantidade à vida.
        this.vida = this.vida + quantidade < 0.0 ? 0.0 : this.vida + quantidade;
        this.status = this.vida > 0.0 ? this.status : Status.MORTO;
        
        /*if(this.vida > 0.0){
            this.status = Status.VIVO;
        }else{
            this.status = Status.MORTO;
        }*/
    }
    
    /**
     * Imprime vida atual do orc.
     * 
     * @return Vida atual do Orc no seguinte formato de exemplo
     * "Vida atual: 110"
     */
    public String toString(){
        // O código abaixo não funciona.
        // Exception: java.util.IllegalFormatConversionException(null)
        /*StringBuilder builder = new StringBuilder();
        builder.append(String.format("Vida atual: %d", this.vida));
        return builder.toString();*/
        
        return "Vida atual: " + this.vida;
    }
    
}
